﻿# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.7.17)
# Database: transporter
# Generation Time: 2017-09-13 18:12:32 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table constants
# ------------------------------------------------------------

LOCK TABLES `constants` WRITE;
/*!40000 ALTER TABLE `constants` DISABLE KEYS */;

INSERT INTO `constants` (`constant_key`, `ENGLISH`, `HINDI`)
VALUES
	('add_company_name_label','Add Company','कंपनी जोड़ें'),
	('add_man_power_label','Add Company Member','कंपनी सदस्य जोड़ें'),
	('add_today_date_search_load','Need Load Today','लोड आज चाहते हैं'),
	('add_today_date_search_vehicle','Need Vehicle Today','वाहन आज चाहते हैं'),
	('add_vehicle_label','Add Vehicle','वाहन जोड़ें'),
	('bad_request_toast','Invalid request. provide correct input and try again.','अमान्य अनुरोध। सही इनपुट प्रदान करें और पुनः प्रयास करें।'),
	('chose_preferred_language_label','Tell us your preferred language..','हमें अपनी पसंदीदा भाषा बताएं..'),
	('company_description_label','Company description','कंपनी का विवरण'),
	('company_members_added','Company members added.','कंपनी के सदस्य जोड़े गए।'),
	('company_name_label','Company\'s registered name (in English)','कंपनी का नाम (अंग्रेजी में)'),
	('confirm_password_label','Confirm PIN','पिन की पुष्टि कीजिये'),
	('connection_error_toast','Request Failed. Check network connection.','अनुरोध विफल| नेटवर्क कनेक्शन की जांच करें|'),
	('customer_happy','Are you happy using Transporter Network?','क्या आप ट्रांसपोर्टर नेटवर्क से खुश हैं?'),
	('date_of_birth_label','Date of birth dd/mm/yyyy','जन्म की तिथि (दिनांक/महीना/बर्ष)'),
	('edit_company_name_label','Edit the company name here. Current company name will be changed to new name. Provide name in English.','कंपनी का नाम यहां सही करें| वर्तमान कंपनी का नाम बदलकर नया नाम बदल जाएगा| अंग्रेजी में नाम प्रदान करें|'),
	('fill_add_member_form_label','Fill the below field to add member. New member will be able to handle all vehicles of the company.','सदस्य जोड़ने के लिए नीचे फ़ील्ड भरें। नए सदस्य कंपनी के वाहनों को जोड़ने/अपडेट/हटाने में सक्षम होंगे|'),
	('fill_add_vehicle_form_label','Fill the below fields to add vehicle. Fill maximum fields for better match in transport network.','वाहन जोड़ने के लिए नीचे फ़ील्ड भरें। ट्रांसपोर्ट नेटवर्क में बेहतर मैच के लिए अधिकतम फ़ील्ड भरें|'),
	('fill_company_name_label','Fill the below field to register company in Transport Network.','ट्रांसपोर्ट नेटवर्क में कंपनी को पंजीकृत करने के लिए नीचे फ़ील्ड भरें।'),
	('fill_form_label','Fill the below fields to proceed','आगे बढ़ने के लिए नीचे फ़ील्ड भरें'),
	('fill_login_form_label','Fill the below fields to login.','लॉग इन करने के लिए नीचे फ़ील्ड भरें'),
	('fill_remove_member_form_label','Fill the below field to remove company member.','कंपनी के सदस्य को हटाने के लिए नीचे फ़ील्ड भरें।'),
	('fill_remove_vehicle_label','Fill the below field to remove vehicle.','वाहन को हटाने के लिए नीचे फ़ील्ड भरें।'),
	('fill_search_load_form_label','Fill the below fields to search load. Fill maximum fields for better match.','भार खोजने के लिए नीचे दिए गए फ़ील्ड भरें। बेहतर परिणाम के लिए अधिकतम फ़ील्ड भरें।'),
	('fill_search_vehicle_form_label','Fill the below fields to search vehicle. Fill maximum fields for better match.','वाहन को खोजने के लिए नीचे दिए गए फ़ील्ड भरें। बेहतर परिणाम के लिए अधिकतम फ़ील्ड भरें।'),
	('fill_update_vehicle_form_label','Fill the below fields to update vehicle.','वाहन को अपडेट करने के लिए नीचे फ़ील्ड भरें।'),
	('history_label','My Vehicles/Trips','मेरे वाहन / यात्राएं'),
	('invalid_capacity_label','Check provided Vehicle Capacity.','प्रदान की गई वाहन क्षमता की जांच करें'),
	('invalid_company_name_label','Check provided company name.','उपलब्ध कंपनी का नाम जांचें'),
	('invalid_date_label','Enter valid date in dd/mm/yyyy format','(दिनांक/महीना/बर्ष) में मान्य तारीख दर्ज करें'),
	('invalid_dob_label','Check provided Date.','प्रदान दिनांक जांचें'),
	('invalid_password_label','Check provided Password.','प्रदान किए गए पासवर्ड की जांच करें'),
	('invalid_phone_number_label','Check provided Phone number.','प्रदान की गई फ़ोन नंबर की जांच करें'),
	('invalid_user_name_label','Check provided Name.','प्रदान किए गए नाम की जांच करें'),
	('invalid_vehicle_number_label','Check provided Vehicle number.','प्रदान की गई वाहन नंबर की जांच करें'),
	('is_empower_to_change_structure','Is empower to add other members','क्या अन्य सदस्यों को जोड़ने के लिए सशक्त है?'),
	('is_linked_to_my_company','Link this vehicle to my company','इस वाहन को मेरी कंपनी से जोड़ें'),
	('load_weight','Load Weight (in tonne)','लोड का भार वजन (टन में)'),
	('login_label','Login','लॉग इन'),
	('logout_confirmation','Do you want to logout from Transport Network?','क्या आप परिवहन नेटवर्क से लॉग आउट करना चाहते हैं?'),
	('logout_label','Logout','लॉग आउट'),
	('make_search_visible','Contact me if match found later.','अगर मैच बाद में मिले तो मुझसे संपर्क करें|'),
	('my_company_label','My Company','मेरी कंपनी'),
	('my_company_members_label','My Company Members','मेरी कंपनी के सदस्य'),
	('my_trips_label','My Trips','मेरी यात्रा'),
	('my_vehicles_label','My Vehicles','मेरे वाहन'),
	('my_vehicle_label','My Vehicles','मेरे वाहन'),
	('new_member_phone_number_label','New member\'s phone number','नए सदस्य का फोन नंबर'),
	('new_vehicle_capacity_label','New Vehicle Capacity in tons',''),
	('no','no','नहीं'),
	('no_company_members_added','No Company Members added.','कोई भी कंपनी के सदस्य जोड़े नहीं गए।'),
	('no_result_found','No match found as per your requirement.','आपकी आवश्यकतानुसार कोई मेल नहीं मिला'),
	('no_vehicle_added','No vehicle added yet.','अभी तक कोई वाहन जोड़ा नहीं गया|'),
	('ok_label','Ok','ठीक है'),
	('open_register_user_activity_label','Go to Registration form','रजिस्ट्रेशन फॉर्म पर जाएं'),
	('other','Other','अन्य'),
	('page_reloaded_successfully','Page reloaded successfully!!','\nपृष्ठ पुनः लोड हो गया है!!'),
	('password_label','PIN of length 6','पिन 6 लंबाई की'),
	('phone_number_label','Phone number','फ़ोन नंबर'),
	('phone_number_to_remove','Phone number of member to remove','निकालने के लिए सदस्य का फोन नंबर'),
	('provide_your_thoughts','Provide your thoughts','अपने विचार प्रदान करें'),
	('refresh_page_label','Reload Page','पृष्ठ पुनः लोड करें'),
	('register_user_label','Register','रजिस्टर'),
	('remove_company_confirmation_msg','Do you want to remove provided company name?','क्या आप प्रदान की गई कंपनी का नाम हटाना चाहते हैं?'),
	('remove_company_label','Remove Company','कंपनी हटाएं'),
	('remove_company_member_confirmation_msg','Do you want to remove provided member from the company?','क्या आप दिए गए सदस्य को कंपनी से निकालना चाहते हैं?'),
	('remove_manpower_label','Remove Company Member','कंपनी सदस्य हटाएं'),
	('remove_vehicle','Remove Vehicle','वाहन हटाएं'),
	('remove_vehicle_confirmation_msg','Do you want to remove provided vehicle from your vehicle list?','क्या आप अपनी वाहन सूची से दिए गए वाहन को निकालना चाहते हैं?'),
	('request_succeeded_toast','Request Succeeded.','अनुरोध सफल'),
	('search_load_label','Search Load','लोड खोजें'),
	('search_manpower_label','Search Manpower','जनशक्ति खोजें'),
	('search_truck_label','Search Vehicle','वाहन खोजें'),
	('select_category','Select Category','श्रेणी का चयन करें'),
	('select_destination_city','Select Destination City','अंतिम शहर का चयन करें'),
	('select_destination_state','Select Destination State','अंतिम राज्य का चयन करें'),
	('select_start_city','Select Start City','प्रारंभ शहर चुनें'),
	('select_start_state','Select Start State','प्रारंभ राज्य चुनें'),
	('select_trip','Select Trip','यात्रा का चयन करें'),
	('select_trip_status','Select Trip Status','ट्रिप स्थिति चुनें'),
	('select_vehicle_current_city','Select Vehicle Current City','वाहन का वर्तमान शहर चुनें'),
	('select_vehicle_current_state','Select Vehicle Current State','वाहन का वर्तमान राज्य चुनें'),
	('select_vehicle_number','Select Vehicle Number','वाहन संख्या चुनें'),
	('select_vehicle_status','Select Vehicle Status','वाहन स्थिति चुनें'),
	('select_work_type','Select what you want to check.','चुनें कि आप क्या देखना चाहते हैं।'),
	('start_date_search_vehicle','Trip start date, when vehicle required','यात्रा की तारीख, जब वाहन की आवश्यकता है'),
	('submit_thoughts','Submit thoughts','विचार सबमिट करें'),
	('tell_us_label','Tell us','हमें बताएँ'),
	('tell_us_your_thoughts_here','Please write your thoughts about Transporter Network here.','कृपया ट्रांसपोर्टर नेटवर्क के बारे में अपने विचार यहां लिखें।'),
	('thanks_for_feedback','Thanks for your valuable feedback. If required, we will connect with you.','आपके बहुमूल्य फ़ीडबैक के लिए धन्यवाद| यदि आवश्यक हो, तो हम आपके साथ जुड़ेंगे।'),
	('title_activity_search_result','We have found match as per your requirement. You may contact the below numbers.','हमने आपकी आवश्यकतानुसार मेल पाया है| आप नीचे दिए गए फोन नंबरों से संपर्क कर सकते हैं |'),
	('todays_date_added_as_start_date','Today\'s date added.','आज की तिथि को जोड़ा गया|'),
	('tonne','tonne','टन'),
	('trip_finished','Finished','यात्रा ख़त्म'),
	('trip_searching_for_vehicle','Searching for Vehicle','वाहन के लिए खोज'),
	('trip_terminated','Terminated','समाप्त'),
	('update_company_name_label','Save Edited Company Name','कंपनी का नाम बदलें'),
	('update_profile_label','Update Profile','प्रोफ़ाइल अपडेट करें'),
	('update_trip_status_label','Update Trip Status','ट्रिप स्थिति अपडेट करें'),
	('update_truck_status_label','Update Vehicle Information','वाहन जानकारी अपडेट करें'),
	('user_name_label','Name','नाम'),
	('vehicle_added','Vehicle(s) added.','वाहन (ओं) को जोड़ा गया है।'),
	('vehicle_available_for_booking','Available For Booking','बुकिंग के लिए उपलब्ध है'),
	('vehicle_booked_for_trip','Booked For Trip','यात्रा के लिए बुक'),
	('vehicle_capacity_label','Vehicle Capacity in Ton','वाहन क्षमता टन में'),
	('vehicle_capacity_required_label','Required vehicle capacity','आवश्यक वाहन क्षमता (टन में)'),
	('vehicle_description_label','Vehicle Description','वाहन के बारे में अधिक बताएं'),
	('vehicle_end_location_label','Preferred End Location','पसंदीदा अंत स्थान'),
	('vehicle_loading','Loading','लोड हो रहा है'),
	('vehicle_number_label','Vehicle Number (in English)','वाहन नंबर (अंग्रेजी में)'),
	('vehicle_on_trip','On Trip','यात्रा पर'),
	('vehicle_start_location_label','Preferred Start Location','पसंदीदा प्रारंभ स्थान'),
	('vehicle_under_maintenance','Under Maintenance','सर्विसिंग जारी'),
	('vehicle_unloading','Unloading','उतराई'),
	('want_team_to_call','Want to talk to transport network team? ','ट्रांसपोर्टर नेटवर्क टीम से बात करना चाहते हैं'),
	('welcome_message','Welcome to Transporter Network.\n1. Search Transportation Vehicle or Loads.\n2. Connect to other transporters.','ट्रांसपोर्टर नेटवर्क में आपका स्वागत है\n1. परिवहन या भार खोजें\n2. अन्य ट्रांसपोर्टरों से कनेक्ट करें'),
	('what_need_improvement','What needs more improvement','किसमें आप सुधार करना चाहते हैं'),
	('what_you_like','What do you liked','आपको क्या पसंद आया'),
	('yes','yes','हाँ');

/*!40000 ALTER TABLE `constants` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
