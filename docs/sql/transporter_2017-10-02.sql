# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.7.17)
# Database: transporter
# Generation Time: 2017-10-02 12:18:19 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table constants
# ------------------------------------------------------------

DROP TABLE IF EXISTS `constants`;

CREATE TABLE `constants` (
  `constant_key` varchar(150) NOT NULL,
  `ENGLISH` varchar(400) NOT NULL,
  `HINDI` varchar(400) NOT NULL,
  PRIMARY KEY (`constant_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `constants` WRITE;
/*!40000 ALTER TABLE `constants` DISABLE KEYS */;

INSERT INTO `constants` (`constant_key`, `ENGLISH`, `HINDI`)
VALUES
	('add_company_name_label','Add Company','कंपनी जोड़ें'),
	('add_man_power_label','Add New Member in Company','कंपनी में नए सदस्य जोड़ें'),
	('add_today_date_search_load','Need Load Today','लोड आज चाहते हैं'),
	('add_today_date_search_vehicle','Need Vehicle Today','वाहन आज चाहते हैं'),
	('add_trip_label','Add Load','लोड जोड़ें'),
	('add_vehicle_label','Add Vehicle','वाहन जोड़ें'),
	('bad_request_toast','Invalid request. provide correct input and try again.','अमान्य अनुरोध। सही इनपुट प्रदान करें और पुनः प्रयास करें।'),
	('chkBox_any_end_city','Any End City','कोई भी अंतिम शहर'),
	('chkBox_any_end_state','Any End State','कोई भी अंतिम राज्य'),
	('chkBox_any_start_city','Any Start City','कोई भी प्रारंभ शहर'),
	('chkBox_any_start_state','Any Start State','कोई भी प्रारंभ राज्य'),
	('company_members_added','Company members added.','कंपनी के सदस्य जोड़े गए।'),
	('company_name_label','Company\'s registered name (in English)','कंपनी का नाम (अंग्रेजी में)'),
	('company_name_short_label','Company\'s Name','कंपनी का नाम'),
	('customer_happy','Are you happy using Transporter Network?','क्या आप ट्रांसपोर्टर नेटवर्क से खुश हैं?'),
	('edit_company_name_button_label','Edit Company Name','कंपनी का नाम सही करें'),
	('edit_company_name_label','Edit the company name here. Current company name will be changed to new name. Provide name in English.','कंपनी का नाम यहां सही करें| वर्तमान कंपनी का नाम बदलकर नया नाम बदल जाएगा| अंग्रेजी में नाम प्रदान करें|'),
	('fill_add_member_form_label','Fill the below field to add member. New member will be able to handle all vehicles of the company.','सदस्य जोड़ने के लिए नीचे फ़ील्ड भरें। नए सदस्य कंपनी के वाहनों को जोड़ने/अपडेट/हटाने में सक्षम होंगे|'),
	('fill_add_trip_form_label','Fill the below fields to add load.','लोड जोड़ने के लिए नीचे दिए गए फ़ील्ड भरें'),
	('fill_add_vehicle_form_label','Fill the below fields to add vehicle. Fill maximum fields for better match in transport network.','वाहन जोड़ने के लिए नीचे फ़ील्ड भरें। ट्रांसपोर्ट नेटवर्क में बेहतर मैच के लिए अधिकतम फ़ील्ड भरें|'),
	('fill_company_name_label','Fill the below field to register company in Transport Network.','ट्रांसपोर्ट नेटवर्क में कंपनी को पंजीकृत करने के लिए नीचे फ़ील्ड भरें।'),
	('fill_form_update_trip_status_label','Select the load and its new status. Vehicles availability will be matched as per the status of the trip.','लोड और इसकी नई स्थिति का चयन करें। लोड की स्थिति के अनुसार वाहन की उपलब्धता का मिलान किया जाएगा।'),
	('fill_remove_member_form_label','Fill the below field to remove company member.','कंपनी के सदस्य को हटाने के लिए नीचे फ़ील्ड भरें।'),
	('fill_remove_vehicle_label','Fill the below field to remove vehicle.','वाहन को हटाने के लिए नीचे फ़ील्ड भरें।'),
	('fill_search_load_form_label','Fill the below fields to search load. Fill maximum fields for better match.','भार खोजने के लिए नीचे दिए गए फ़ील्ड भरें। बेहतर परिणाम के लिए अधिकतम फ़ील्ड भरें।'),
	('fill_search_vehicle_form_label','Fill the below fields to search vehicle.','वाहन को खोजने के लिए नीचे दिए गए फ़ील्ड भरें।'),
	('fill_update_vehicle_form_label','Fill the below fields to update vehicle.','वाहन को अपडेट करने के लिए नीचे फ़ील्ड भरें।'),
	('invalid_capacity_label','Check provided Vehicle Capacity.','प्रदान की गई वाहन क्षमता की जांच करें'),
	('invalid_company_name_label','Check provided company name.','उपलब्ध कंपनी का नाम जांचें'),
	('invalid_date_label','Enter valid date in dd/mm/yyyy format','(दिनांक/महीना/बर्ष) में मान्य तारीख दर्ज करें'),
	('invalid_vehicle_number_label','Check provided Vehicle number.','प्रदान की गई वाहन नंबर की जांच करें'),
	('is_empower_to_change_structure','Click here if new member can add more members in the company.','यहां क्लिक करें यदि नया सदस्य कंपनी में अधिक सदस्य जोड़ सकता है|'),
	('load_weight','Load Weight (in tonne)','लोड का भार वजन (टन में)'),
	('logout_confirmation','Do you want to logout from Transport Network?','क्या आप परिवहन नेटवर्क से लॉग आउट करना चाहते हैं?'),
	('logout_label','Logout','लॉग आउट'),
	('my_company_label','My Company','मेरी कंपनी'),
	('my_trips_label','My Load','मेरा लोड'),
	('my_vehicles_label','My Vehicles','मेरे वाहन'),
	('new_member_phone_number_label','New member\'s phone number','नए सदस्य का फोन नंबर'),
	('new_vehicle_capacity_label','New Vehicle Capacity in tons','नई वाहन क्षमता (टन में)'),
	('no','no','नहीं'),
	('no_company_members_added','No Company Members added.','कोई भी कंपनी के सदस्य जोड़े नहीं गए।'),
	('no_result_found','No match found as per your requirement.','आपकी आवश्यकतानुसार कोई मेल नहीं मिला'),
	('no_trip_added','No load searching for vehicle.','वाहन की खोज के लिए कोई लोड नहीं है|'),
	('no_vehicle_added','No vehicle added yet.','अभी तक कोई वाहन जोड़ा नहीं गया|'),
	('phone_number_to_remove','Phone number of member to remove','निकालने के लिए सदस्य का फोन नंबर'),
	('refresh_page_label','Reload Page','पृष्ठ पुनः लोड करें'),
	('remove_company_confirmation_msg','Do you want to remove provided company name? (If you are the last member empowered to add other members then all other members and vehicles will be automatically dissociated from the Company.)','क्या आप प्रदान की गई कंपनी का नाम हटाना चाहते हैं?'),
	('remove_company_label','Remove Company','कंपनी हटाएं'),
	('remove_company_member_confirmation_msg','Do you want to remove provided member from the company?','क्या आप दिए गए सदस्य को कंपनी से निकालना चाहते हैं?'),
	('remove_manpower_label','Remove Company Member','कंपनी सदस्य हटाएं'),
	('remove_vehicle','Remove Vehicle','वाहन हटाएं'),
	('remove_vehicle_confirmation_msg','Do you want to remove provided vehicle from your vehicle list?','क्या आप अपनी वाहन सूची से दिए गए वाहन को निकालना चाहते हैं?'),
	('same_edit_company_name','Company name provided is same as present name. Provide different name.','उपलब्ध कंपनी का नाम वर्तमान नाम के समान है। अलग नाम प्रदान करें|'),
	('search_load_for_added_vehicles_label','Search Load for added Vehicle(s)','वाहन के लिए लोड खोजें'),
	('search_load_label','Search Load','लोड खोजें'),
	('search_truck_label','Search Vehicle','वाहन खोजें'),
	('search_vehicle_for_added_trips_label','Search Vehicle for added Load','लोड के लिए वाहन खोजें'),
	('select_destination_city','Select Destination City','अंतिम शहर का चयन करें'),
	('select_destination_state','Select Destination State','अंतिम राज्य का चयन करें'),
	('select_start_city','Select Start City','प्रारंभ शहर चुनें'),
	('select_start_state','Select Start State','प्रारंभ राज्य चुनें'),
	('select_trip','Select Load Trip','लोड का चयन करें'),
	('select_trip_from_spinner','Select added load by clicking below and get the contact number of vehicle owners searching load.','नीचे क्लिक करके लोड चुनें और लोड खोज रहे वाहन मालिकों का संपर्क नंबर प्राप्त करें।'),
	('select_trip_status','Select Trip Status','ट्रिप स्थिति चुनें'),
	('select_vehicle_current_city','Select New Start City','वाहन का वर्तमान शहर चुनें'),
	('select_vehicle_current_state','Select New Start State','वाहन का वर्तमान राज्य चुनें'),
	('select_vehicle_from_spinner','Select added vehicle by clicking below and get the contact number of transporters searching vehicle.','नीचे क्लिक करके जोड़ा गया वाहन चुनें और वाहन की खोज करने वाले ट्रांसपोर्टरों का संपर्क नंबर प्राप्त करें।'),
	('select_vehicle_number','Select Vehicle Number','वाहन संख्या चुनें'),
	('select_vehicle_status','Select Vehicle Status','वाहन स्थिति चुनें'),
	('start_date_search_vehicle','Trip start date, when vehicle required','यात्रा की तारीख, जब वाहन की आवश्यकता है'),
	('title_activity_search_result','We have found match as per your requirement. You may contact the below numbers.','हमने आपकी आवश्यकतानुसार मेल पाया है| आप नीचे दिए गए फोन नंबरों से संपर्क कर सकते हैं |'),
	('todays_date_added_as_start_date','Today\'s date added.','आज की तिथि को जोड़ा गया|'),
	('tonne','ton','टन'),
	('trips_added','Load added. List not showing finished or cancelled load.','लोड जोड़ा गया है| सूची समाप्त या रद्द लोड नहीं दिखा रही हैं|'),
	('update_company_name_label','Save Edited Company Name','कंपनी का नाम बदलें'),
	('update_trip_status_label','Update Load Status','लोड स्थिति अपडेट करें'),
	('update_truck_status_label','Update Vehicle Information','वाहन जानकारी अपडेट करें'),
	('vehicle_added','Vehicle(s) added.','वाहन(ओं) को जोड़ा गया है।'),
	('vehicle_capacity_label','Vehicle Capacity in Ton','वाहन क्षमता टन में'),
	('vehicle_capacity_required_label','Required vehicle capacity','आवश्यक वाहन क्षमता (टन में)'),
	('vehicle_number_label','Vehicle Number (in English)','वाहन नंबर (अंग्रेजी में)'),
	('want_team_to_call','Want to talk to transport network team? ','ट्रांसपोर्टर नेटवर्क टीम से बात करना चाहते हैं'),
	('yes','yes','हाँ');

/*!40000 ALTER TABLE `constants` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
