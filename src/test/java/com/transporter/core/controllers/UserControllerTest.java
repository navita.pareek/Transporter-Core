package com.transporter.core.controllers;

import com.transporter.core.constants.StringConstants;
import com.transporter.core.services.LogService;
import com.transporter.core.services.UserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.HashMap;
import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class UserControllerTest {

    @InjectMocks
    private UserController userController;

    @Mock
    private UserService userService;

    @Mock
    private LogService logService;

    @Test
    public void shouldTerminateForRegisterUserWithMissingPassword(){

        Map<String, String> payload = new HashMap<>();
        payload.put("phoneNumber", "89898989");
        payload.put("name", "nav");
        payload.put("dateOfBirth", "09/09/1909");

        userController.registerUser(payload);
        verify(userService, times(0)).registerUser(any());
    }

    @Test
    public void shouldTerminateForRegisterUserWithMissingDateOfBirth(){

        Map<String, String> payload = new HashMap<>();
        payload.put("phoneNumber", "89898989");
        payload.put("name", "nav");
        payload.put("password", "password");

        userController.registerUser(payload);
        verify(userService, times(0)).registerUser(any());
    }

    @Test
    public void shouldTerminateForRegisterUserWithMissingPhoneNumber(){

        Map<String, String> payload = new HashMap<>();
        payload.put("password", "password");
        payload.put("name", "nav");
        payload.put("dateOfBirth", "09/09/1909");

        userController.registerUser(payload);
        verify(userService, times(0)).registerUser(any());
    }

    @Test
    public void shouldTerminateForRegisterUserWithInValidLanguage(){

        when(userService.registerUser(any())).thenReturn("TRUE");

        Map<String, String> payload = new HashMap<>();
        payload.put("phoneNumber", "909089089");
        payload.put("password", "password");
        payload.put("name", "nav");
        payload.put("dateOfBirth", "09/09/1909");
        payload.put("dateOfBirth", "09/09/1909");
        payload.put("language", "FRENCH");
        String result = userController.registerUser(payload);
        assertThat(result, is("REQUIRED_INPUTS_ARE_NOT_PROVIDED"));
        verify(userService, times(0)).registerUser(any());
    }

    @Test
    public void shouldTerminateForRegisterUserWithMissingLanguage(){

        when(userService.registerUser(any())).thenReturn("TRUE");

        Map<String, String> payload = new HashMap<>();
        payload.put("phoneNumber", "909089089");
        payload.put("password", "password");
        payload.put("name", "nav");
        payload.put("dateOfBirth", "09/09/1909");
        payload.put("dateOfBirth", "09/09/1909");
        String result = userController.registerUser(payload);
        assertThat(result, is("REQUIRED_INPUTS_ARE_NOT_PROVIDED"));
        verify(userService, times(0)).registerUser(any());
    }

    @Test
    public void shouldReturnTrueForRegisterUserWithValidInputs(){

        when(userService.registerUser(any())).thenReturn("TRUE");

        Map<String, String> payload = new HashMap<>();
        payload.put("phoneNumber", "909089089");
        payload.put("password", "password");
        payload.put("name", "nav");
        payload.put("dateOfBirth", "09/09/1909");
        payload.put("dateOfBirth", "09/09/1909");
        payload.put("language", "HINDI");
        String result = userController.registerUser(payload);
        assertThat(result, is("TRUE"));
        verify(userService, times(1)).registerUser(any());
    }

    @Test
    public void shouldReturnFalseForAlreadyRegisterUser(){

        when(userService.isUserRegistered("1234567890")).thenReturn(true);

        Map<String, String> payload = new HashMap<>();
        payload.put("phoneNumber", "1234567890");
        payload.put("password", "password");
        payload.put("name", "nav");
        payload.put("dateOfBirth", "09/09/1909");

        userController.registerUser(payload);
        verify(userService, times(1)).isUserRegistered(any());
        verify(userService, times(0)).registerUser(any());
    }
}
