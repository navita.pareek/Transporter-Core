package com.transporter.core.tools;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import static org.assertj.core.api.Assertions.not;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.equalTo;

@RunWith(MockitoJUnitRunner.class)
public class StringMagicianTest {

    @Test
    public void getDifferentStringAfterEncryption(){
        String inputString  = "Test_Encryption";
        String result = StringMagician.getInstance().getEncryptedString(inputString);

        assertThat((result != inputString), is(true));
    }
}
