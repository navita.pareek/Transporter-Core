package com.transporter.core.services;

import com.transporter.core.daos.LogDao;
import com.transporter.core.daos.UserDao;
import com.transporter.core.models.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceTest {

    @InjectMocks
    private UserService userService;

    @Mock
    private LogDao logDao;

    @Mock
    private UserDao userDao;

    @Test
    public void shouldCallSavePreferredLanguageForValidInput(){

        String result = userService.savePreferredLanguage(1,"en");
        assertThat(result, is("TRUE"));
        verify(userDao, times(1)).savePreferredLanguage(1, "en");
    }

    @Test
    public void shouldCallRegisterUserForValidInputWithOutLanguage(){
        User user = new User("909089090", "09/09/1999","sjdjsdhc");
        userService.registerUser(user);
        verify(userDao, times(1)).registerUser("909089090", "09/09/1999","sjdjsdhc", null);
    }

    @Test
    public void shouldCallRegisterUserForValidUserWithLanguage(){
        User user = new User("909089090", "09/09/1999","password");
        user.setLanguage("language");
        userService.registerUser(user);
        verify(userDao, times(1)).registerUser("909089090", "09/09/1999","password", "language");
    }

    @Test
    public void shouldReturnTrueForRegisteredUser(){

        when(userDao.isUserRegistered("1234567890")).thenReturn(1);
        boolean result = userService.isUserRegistered("1234567890");
        assertThat(result, is(true));
        verify(userDao, times(1)).isUserRegistered("1234567890");
    }

    @Test
    public void shouldReturnFalseForUnRegisteredUser(){

        when(userDao.isUserRegistered("1234567890")).thenReturn(0);
        boolean result = userService.isUserRegistered("1234567890");
        assertThat(result, is(false));
        verify(userDao, times(1)).isUserRegistered("1234567890");
    }
}
