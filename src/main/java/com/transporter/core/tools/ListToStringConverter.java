package com.transporter.core.tools;

import java.util.List;

public class ListToStringConverter {
    private static ListToStringConverter ourInstance = new ListToStringConverter();

    public static ListToStringConverter getInstance() {
        return ourInstance;
    }

    private ListToStringConverter() {
    }

    public <T>  String convert(List<T> list) {
        StringBuilder result = new StringBuilder();
        for(int index = 0; index < list.size(); index++){
            if(index == (list.size() - 1)) {
                result.append(list.get(index).toString());
            }
            else {
                result.append(list.get(index).toString() + ",");
            }
        }
        return result.toString();
    }
}
