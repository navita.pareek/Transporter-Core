package com.transporter.core.tools;

public class StringToJsonSerializedString {

    public static String ConvertToJsonSerializedString(String input, String tag) {
        StringBuilder result = new StringBuilder();
        result.append("{\"" + tag + "\"");
        result.append(":");
        result.append("[");
        result.append(input);
        result.append("]");
        result.append("}");

        return result.toString();
    }
}
