package com.transporter.core.tools;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class StringMagician {
    private static StringMagician ourInstance = new StringMagician();

    private static final String SALT = "turuck-dhina-dhin";

    public static StringMagician getInstance() {
        return ourInstance;
    }

    private StringMagician() {
    }

    private static String generateHash(String input) {
        StringBuilder hash = new StringBuilder();

        try {
            MessageDigest sha = MessageDigest.getInstance("SHA-1");
            byte[] hashedBytes = sha.digest(input.getBytes());
            char[] digits = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
                    'a', 'b', 'c', 'd', 'e', 'f' };
            for (int idx = 0; idx < hashedBytes.length; ++idx) {
                byte b = hashedBytes[idx];
                hash.append(digits[(b & 0xf0) >> 4]);
                hash.append(digits[b & 0x0f]);
            }
        } catch (NoSuchAlgorithmException e) {
            return input;
        }

        return hash.toString();
    }

    public String getEncryptedString(String inputString){
        String saltedPassword = SALT + inputString;
        return generateHash(saltedPassword);
    }

}
