package com.transporter.core;

import com.transporter.core.daos.*;
import org.skife.jdbi.v2.DBI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;

@SpringBootApplication
@EnableWebSecurity
public class CoreApplication {

	@Autowired
	private DBI dbi;

	public static void main(String[] args) {
		SpringApplication.run(CoreApplication.class, args);
	}

    @Bean
    public UserDao getUserDao() {
        return dbi.onDemand(UserDao.class);
    }

	@Bean
	public LogDao getLogDao() {
		return dbi.onDemand(LogDao.class);
	}

	@Bean
	public VehicleDao getVehicleDao() {
		return dbi.onDemand(VehicleDao.class);
	}

	@Bean
	public SupportDao getSupportDao() {
		return dbi.onDemand(SupportDao.class);
	}

	@Bean
	public CompanyDao getCompanyDao() {
		return dbi.onDemand(CompanyDao.class);
	}

	@Bean
	public TripDao getTripDao() {
		return dbi.onDemand(TripDao.class);
	}
}
