package com.transporter.core.mapper;

import com.transporter.core.models.User;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class UserMapper implements ResultSetMapper<User> {

    @Override
    public User map(int index, ResultSet r, StatementContext ctx) throws SQLException {
        User user = new User(r.getString("phone_number"));
        user.setLanguage(r.getString("language"));
        user.setUserTnPoints(r.getInt("user_tn_points"));
        return user;
    }
}
