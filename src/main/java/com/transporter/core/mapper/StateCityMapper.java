package com.transporter.core.mapper;

import com.transporter.core.models.StateCity;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class StateCityMapper implements ResultSetMapper<StateCity> {

    @Override
    public StateCity map(int index, ResultSet r, StatementContext ctx) throws SQLException {
        StateCity result = new StateCity(r.getString("state_or_union_territory"), r.getString("city"));
        result.setId(r.getInt("id"));
        return result;
    }
}
