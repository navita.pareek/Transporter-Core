package com.transporter.core.mapper;

import com.transporter.core.models.Trip;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class TripMapper implements ResultSetMapper<Trip> {

    @Override
    public Trip map(int index, ResultSet r, StatementContext ctx) throws SQLException {
        Trip trip = new Trip();
        trip.setStartStateIndex(trip.getStateIndex(r.getString("start_location")));
        trip.setStartCityIndex(trip.getCityIndex(r.getString("start_location")));
        trip.setEndStateIndex(trip.getStateIndex(r.getString("end_location")));
        trip.setEndCityIndex(trip.getCityIndex(r.getString("end_location")));
        trip.setStartDate(r.getString("start_date"));
        trip.setVehicleCapacityRequired(r.getFloat("vehicle_capacity_required"));
        trip.setUserId(r.getInt("user_id"));
        trip.setTripStatus(r.getString("status"));
        return trip;
    }
}
