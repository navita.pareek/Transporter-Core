package com.transporter.core.mapper;

import com.transporter.core.models.Vehicle;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class VehicleMapper implements ResultSetMapper<Vehicle> {

    @Override
    public Vehicle map(int index, ResultSet r, StatementContext ctx) throws SQLException {
        Vehicle vehicle = new Vehicle(r.getString("vehicle_number"), r.getFloat("capacity"));
        vehicle.setStartLocation(r.getString("start_location"));
        vehicle.setEndLocation(r.getString("end_location"));
        vehicle.setCompanyId(r.getInt("company_id"));
        vehicle.setUserId(r.getInt("user_id"));
        vehicle.setStatus(r.getString("status"));
        return vehicle;
    }
}
