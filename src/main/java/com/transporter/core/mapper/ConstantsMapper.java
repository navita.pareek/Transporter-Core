package com.transporter.core.mapper;

import com.transporter.core.models.Constants;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ConstantsMapper implements ResultSetMapper<Constants> {

    @Override
    public Constants map(int index, ResultSet r, StatementContext ctx) throws SQLException {
        return new Constants(r.getString(1), r.getString(2));
    }
}
