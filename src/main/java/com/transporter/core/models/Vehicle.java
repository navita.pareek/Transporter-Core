package com.transporter.core.models;

import com.transporter.core.constants.StringConstants;

public class Vehicle {

    private String vehicleNumber;
    private float capacity;
    private String endLocation;
    private String status;
    private String startLocation;
    private String isLinkedToCompany;
    private int companyId;
    private int userId;

    public Vehicle(String vehicleNumber, float capacity) {
        this.vehicleNumber = vehicleNumber;
        this.capacity = capacity;
    }

    public Vehicle(String vehicleNumber) {
        this.vehicleNumber = vehicleNumber;
    }

    public void setStartLocation(String startLocation) {
        this.startLocation = startLocation;
    }

    public void setEndLocation(String endLocation) {
        this.endLocation = endLocation;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getVehicleNumber(){
        return vehicleNumber;
    }

    public float getCapacity(){
        return capacity;
    }

    public String getStartLocation(){
        return startLocation;
    }

    public String getEndLocation(){
        return endLocation;
    }

    public String getStatus(){
        return status;
    }

    public void setIsLinkedToCompany(String isLinkedToCompany) {
        this.isLinkedToCompany = isLinkedToCompany;
    }

    public String getIsLinkedToCompany() {
        return isLinkedToCompany;
    }

    public void setCompanyId(int companyId) {
        this.companyId = companyId;
    }

    public int getCompanyId() {
        return companyId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    @Override
    public String toString(){
        StringBuilder result = new StringBuilder();
        result.append("{");
        result.append("\"" + StringConstants.vehicleNumber + "\"");
        result.append(":");
        result.append("\"" + vehicleNumber + "\"");
        result.append(",");

        result.append("\"" + StringConstants.capacity + "\"");
        result.append(":");
        result.append("\"" + capacity + "\"");
        result.append(",");

        result.append("\"" + StringConstants.isLinkedToCompany + "\"");
        result.append(":");
        result.append("\"" + isLinkedToCompany + "\"");
        result.append(",");


        result.append("\"" + StringConstants.startLocation + "\"");
        result.append(":");
        result.append("\"" + startLocation + "\"");
        result.append(",");

        result.append("\"" + StringConstants.endLocation + "\"");
        result.append(":");
        result.append("\"" + endLocation + "\"");
        result.append(",");

        result.append("\"" + StringConstants.status + "\"");
        result.append(":");
        result.append("\"" + status + "\"");
        result.append("}");
        return result.toString();
    }

    @Override
    public int hashCode() {
        return this.vehicleNumber.hashCode();
    }

    public boolean equals(Object nextVehicle){
        Vehicle vehicle = (Vehicle) nextVehicle;
        if(vehicle.getVehicleNumber().equals(this.vehicleNumber)){
            return true;
        }
        return false;
    }
}
