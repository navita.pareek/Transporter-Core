package com.transporter.core.models;

public class Constants {
    String constantsKey;
    String value;

    public Constants(String constantsKey, String value){
        this.constantsKey = constantsKey;
        this.value = value;
    }

    @Override
    public String toString(){
        return this.constantsKey + ':' + this.value;
    }
}
