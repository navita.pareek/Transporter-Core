package com.transporter.core.models;

import com.transporter.core.constants.StringConstants;

public class Trip {
    private String startDate;
    private String startStateIndex;
    private String startCityIndex;
    private String endStateIndex;
    private String endCityIndex;
    private float vehicleCapacityRequired;
    private String tripStatus;
    private String tripDescription;
    private int userId;

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public void setVehicleCapacityRequired(float vehicleCapacityRequired) {
        this.vehicleCapacityRequired = vehicleCapacityRequired;
    }

    public void setTripStatus(String tripStatus) {
        this.tripStatus = tripStatus;
    }

    public void setTripDescription(String tripDescription) {
        this.tripDescription = tripDescription;
    }

    public String getStartDate() {
        return startDate;
    }

    public float getVehicleCapacityRequired() {
        return vehicleCapacityRequired;
    }

    public String getTripStatus() {
        return tripStatus;
    }

    public String getTripDescription() {
        return tripDescription;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getUserId() {
        return userId;
    }

    public String getStartStateIndex() {
        return startStateIndex;
    }

    public void setStartStateIndex(String startStateIndex) {
        this.startStateIndex = startStateIndex;
    }

    public String getStartCityIndex() {
        return startCityIndex;
    }

    public void setStartCityIndex(String startCityIndex) {
        this.startCityIndex = startCityIndex;
    }

    public String getEndStateIndex() {
        return endStateIndex;
    }

    public void setEndStateIndex(String endStateIndex) {
        this.endStateIndex = endStateIndex;
    }

    public String getEndCityIndex() {
        return endCityIndex;
    }

    public void setEndCityIndex(String endCityIndex) {
        this.endCityIndex = endCityIndex;
    }

    @Override
    public String toString(){
        StringBuilder result = new StringBuilder();
        result.append("{");
        result.append("\"" + StringConstants.startDate + "\"");
        result.append(":");
        result.append("\"" + startDate + "\"");
        result.append(",");

        result.append("\"" + StringConstants.startLocation + "\"");
        result.append(":");
        result.append("\"" + startStateIndex + ':' + startCityIndex + "\"");
        result.append(",");

        result.append("\"" + StringConstants.vehicleCapacityRequired + "\"");
        result.append(":");
        result.append("\"" + vehicleCapacityRequired + "\"");
        result.append(",");

        result.append("\"" + StringConstants.endLocation + "\"");
        result.append(":");
        result.append("\"" + endStateIndex + ':' + endCityIndex  + "\"");
        result.append(",");

        result.append("\"" + StringConstants.status + "\"");
        result.append(":");
        result.append("\"" + tripStatus + "\"");
        result.append("}");

        return result.toString();
    }

    public String getStateIndex(String location) {
        return location.substring(0, location.indexOf(':'));
    }

    public String getCityIndex(String location) {
        return location.substring(location.indexOf(':') + 1);
    }


}
