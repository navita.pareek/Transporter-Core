package com.transporter.core.models;

import com.transporter.core.constants.StringConstants;

import java.time.Instant;
import java.util.Calendar;
import java.util.Date;

public class LogEntry {

    private final String className;
    private final int responseSize;
    private final String phoneNumber;
    private final String response;
    private final String request;
    private final String method;
    private final String message;
    private final String logLevel;

    public LogEntry(String className, String logLevel, String message, String method, String request, String response, String phoneNumber, int responseSize) {
        this.className =  className;
        this.logLevel =  logLevel;
        this.message =  message;
        this.method =  method;
        this.request =  request;
        this.response =  response;
        this.phoneNumber =  phoneNumber;
        this.responseSize = responseSize;
    }

    public String getClassName() {
        return className;
    }

    public int getResponseSize() {
        return responseSize;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public String getResponse() {
        return response;
    }

    public String getRequest() {
        return request;
    }

    public String getMethod() {
        return method;
    }

    public String getMessage() {
        return message;
    }

    public String getLogLevel() {
        return logLevel;
    }

    @Override
    public String toString(){
        StringBuilder result = new StringBuilder();
        result.append("{");
        result.append("\"" + "dateTime" + "\"");
        result.append(":");
        result.append("\"" + Instant.now().toString()+ "\"");
        result.append(",");

        result.append("\"" + "className" + "\"");
        result.append(":");
        result.append("\"" + this.className + "\"");
        result.append(",");

        result.append("\"" + "method" + "\"");
        result.append(":");
        result.append("\"" + this.method + "\"");
        result.append(",");

        result.append("\"" + "phoneNumber" + "\"");
        result.append(":");
        result.append("\"" + this.phoneNumber + "\"");
        result.append(",");

        result.append("\"" + "message" + "\"");
        result.append(":");
        result.append("\"" + this.message + "\"");
        result.append(",");

        result.append("\"" + "request" + "\"");
        result.append(":");
        result.append("\"" + this.request + "\"");
        result.append(",");

        result.append("\"" + "response" + "\"");
        result.append(":");
        result.append("\"" + this.response + "\"");
        result.append(",");

        result.append("\"" + "responseSize" + "\"");
        result.append(":");
        result.append("\"" + this.responseSize + "\"");
        result.append("}");
        return result.toString();
    }
}
