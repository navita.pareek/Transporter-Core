package com.transporter.core.models;

import com.transporter.core.constants.StringConstants;

public class User {

    private String phoneNumber;
    private String dateOfBirth;
    private String password;
    private String language;
    private String companyName;
    private int userId;
    private int userTnPoints;
    private String referencePhoneNumber;

    public User(String phoneNumber){
        this.phoneNumber = phoneNumber;
    }

    public User(String phoneNumber, String dateOfBirth, String password){
        this.phoneNumber = phoneNumber;
        this.dateOfBirth = dateOfBirth;
        this.password = password;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public String getPassword() {
        return password;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getLanguage() {
        return language;
    }

    @Override
    public String toString(){
        StringBuilder result = new StringBuilder();
        result.append("{");
        result.append("\"" + StringConstants.phoneNumber + "\"");
        result.append(":");
        result.append("\"" + phoneNumber + "\"");
        result.append(",");

        result.append("\"" + StringConstants.dateOfBirth + "\"");
        result.append(":");
        result.append("\"" + dateOfBirth + "\"");
        result.append(",");

        result.append("\"" + StringConstants.password + "\"");
        result.append(":");
        result.append("\"" + password + "\"");
        result.append(",");

        result.append("\"" + StringConstants.language + "\"");
        result.append(":");
        result.append("\"" + language + "\"");
        result.append(",");

        result.append("\"" + StringConstants.userTnPoints + "\"");
        result.append(":");
        result.append("\"" + userTnPoints + "\"");
        result.append(",");

        result.append("\"" + StringConstants.companyName + "\"");
        result.append(":");
        result.append("\"" + companyName + "\"");
        result.append("}");
        return result.toString();
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getUserId() {
        return userId;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public int getUserTnPoints() {
        return userTnPoints;
    }

    public void setUserTnPoints(int userTnPoints) {
        this.userTnPoints = userTnPoints;
    }

    public void setReferencePhoneNumber(String referencePhoneNumber) {
        this.referencePhoneNumber = referencePhoneNumber;
    }

    public String getReferencePhoneNumber() {
        return referencePhoneNumber;
    }
}
