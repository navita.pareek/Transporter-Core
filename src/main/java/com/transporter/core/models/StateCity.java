package com.transporter.core.models;

public class StateCity {
    int id;
    String state;
    String city;

    public StateCity(String state, String city){
        this.state = state;
        this.city = city;
    }

    public void setId(int id){
        this.id = id;
    }

    @Override
    public String toString(){
        return this.state + ':' + this.city;
    }
}
