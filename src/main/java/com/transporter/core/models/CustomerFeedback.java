package com.transporter.core.models;

public class CustomerFeedback {
    String phoneNumber;
    String isHappy;
    private boolean wantToConnect;

    public CustomerFeedback(String phoneNumber, String isHappy){
        this.phoneNumber = phoneNumber;
        this.isHappy = isHappy;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public String getIsHappy() {
        return isHappy;
    }

    public void setWantToConnect(boolean wantToConnect) {
        this.wantToConnect = wantToConnect;
    }

    public boolean getWantToConnect() {
        return wantToConnect;
    }
}
