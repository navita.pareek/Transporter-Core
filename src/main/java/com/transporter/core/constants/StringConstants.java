package com.transporter.core.constants;

public final class StringConstants {

    public static final String SUCCESS = "TRUE";

    public static final String REQUIRED_INPUTS_ARE_NOT_PROVIDED = "REQUIRED_INPUTS_ARE_NOT_PROVIDED";
    public static final String USER_NOT_REGISTERED = "USER_NOT_REGISTERED";
    public static final String USER_ALREADY_REGISTERED = "USER_ALREADY_REGISTERED";
    public static final String VEHICLE_ALREADY_REGISTERED = "VEHICLE_ALREADY_REGISTERED";
    public static final String SERVER_UNDER_MAINTENANCE = "SERVER_UNDER_MAINTENANCE";
    public static final String INVALID_INFORMATION_PROVIDED = "INVALID_INFORMATION_PROVIDED";
    public static final String USER_NOT_IN_SAME_COMPANY = "USER_NOT_IN_SAME_COMPANY";
    public static final String NEW_CUSTOMER_SUPPORT = "NEW_CUSTOMER_SUPPORT";

    public static final String COMPANY_REGISTERED = "COMPANY_REGISTERED";
    public static final String COMPANY_NOT_REGISTERED = "COMPANY_NOT_REGISTERED";

    public static final String DUPLICATE_REQUEST = "DUPLICATE_REQUEST";
    public static final String USER_NOT_ELIGIBLE_TO_ADD_MANPOWER = "USER_NOT_ELIGIBLE_TO_ADD_MANPOWER";
    public static final String USER_IS_ALREADY_WORKING_FOR_SOME_COMPANY = "USER_IS_ALREADY_WORKING_FOR_SOME_COMPANY";
    public static final String VEHICLE_NOT_REGISTERED = "VEHICLE_NOT_REGISTERED";
    public static final String TRIP_ALREADY_EXISTS = "TRIP_ALREADY_EXISTS";

    public static final String phoneNumber = "phoneNumber";
    public static final String referencePhoneNumber = "referencePhoneNumber";
    public static final String dateOfBirth = "dateOfBirth";
    public static final String password = "password";
    public static final String oldPassword = "oldPassword";
    public static final String language = "language";
    public static final String users = "users";
    public static final String userTnPoints = "userTnPoints";

    public static final String companyName = "companyName";

    public static final String vehicleNumber = "vehicleNumber";
    public static final String capacity = "capacity";
    public static final String isLinkedToCompany = "isLinkedToCompany";
    public static final String status = "status";
    public static final String currentState = "currentState";
    public static final String currentCity = "currentCity";

    public static final String newManpowerPhoneNumber = "newManpowerPhoneNumber";
    public static final String isEmpowerForStructureChange = "isEmpowerForStructureChange";

    public static final String startDate = "startDate";
    public static final String startState = "startState";
    public static final String startCity = "startCity";
    public static final String endState = "endState";
    public static final String endCity = "endCity";
    public static final String vehicleCapacityRequired = "vehicleCapacityRequired";
    public static final String tripDescription = "tripDescription";
    public static final String newStatus = "newStatus";
    public static final String startLocation = "startLocation";
    public static final String endLocation = "endLocation";
    public static final String vehicles = "vehicles";

    public static final String vehicle_available_for_booking = "1";
    public static final String vehicle_unloading = "5";

    public static final String trip_searching_for_vehicle = "1";
    public static final String trip_finished = "6";
    public static final String trips = "trips";
    public static final String isHappy = "isHappy";
    public static final String wantToConnect = "wantToConnect";
    public static final int minContacts = 200;
    public static final String trip_cancelled = "7";

    public static final String FLOAT_PRECISION = "%.3f";
    public static final int referencePoints = 5;
    public static final int vehicleAddPoints = 1;
    public static final int tripAddPoints = 1;
}
