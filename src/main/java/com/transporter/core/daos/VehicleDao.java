package com.transporter.core.daos;

import com.transporter.core.mapper.VehicleMapper;
import com.transporter.core.models.Vehicle;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.Define;
import org.skife.jdbi.v2.sqlobject.customizers.Mapper;
import org.skife.jdbi.v2.sqlobject.stringtemplate.UseStringTemplate3StatementLocator;

import java.util.List;

@UseStringTemplate3StatementLocator
public interface VehicleDao {

    @SqlQuery("Select Count(*) FROM vehicles WHERE upper(vehicle_number) = '<vehicleNumber>' AND is_linked_to_user = 1")
    int isVehicleRegistered(@Define("vehicleNumber") String vehicleNumber);

    @SqlUpdate("INSERT INTO vehicles(user_id, vehicle_number, capacity, company_id, start_location, end_location, status) VALUES(<userId>, '<vehicleNumber>', <capacity>, <companyId>, '<startLocation>', '<endLocation>', '<status>')")
    void addVehicle(@Define("userId") int userId, @Define("vehicleNumber") String vehicleNumber, @Define("capacity") float capacity, @Define("startLocation") String startLocation, @Define("endLocation") String endLocation, @Define("status") String currentStatus, @Define("companyId") int companyId);

    @SqlQuery("Select * FROM vehicles WHERE user_id = <userId> AND company_id IS NULL AND is_linked_to_user = 1")
    @Mapper(VehicleMapper.class)
    List<Vehicle> getUserVehiclesByUserIdNotLinkedToCompany(@Define("userId") int userId);

    @SqlQuery("Select * FROM vehicles WHERE company_id = <companyId> AND is_linked_to_user = 1")
    @Mapper(VehicleMapper.class)
    List<Vehicle> getUserVehiclesByCompanyId(@Define("companyId") int companyId);

    @SqlUpdate("INSERT INTO vehicles(user_id, vehicle_number, capacity, start_location, end_location, status) VALUES(<userId>, '<vehicleNumber>', <capacity>, '<startLocation>', '<endLocation>', '<status>')")
    void addVehicle(@Define("userId") int userId, @Define("vehicleNumber") String vehicleNumber, @Define("capacity") float capacity, @Define("startLocation") String startLocation, @Define("endLocation") String endLocation, @Define("status") String currentStatus);

    @SqlUpdate("UPDATE vehicles SET start_location = '<startLocation>', end_location = '<endLocation>', status = '<status>', capacity = <capacity> WHERE vehicle_number = '<vehicleNumber>' AND is_linked_to_user = 1")
    void updateVehicle(@Define("vehicleNumber") String vehicleNumber, @Define("startLocation") String startLocation, @Define("endLocation") String endLocation, @Define("status") String currentStatus, @Define("capacity") float capacity);

    @SqlQuery("Select * FROM vehicles WHERE start_location LIKE '<startState>' AND end_location LIKE '<endState>' AND status = '<status>' AND capacity >= <capacity> AND is_linked_to_user = 1 ORDER BY capacity")
    @Mapper(VehicleMapper.class)
    List<Vehicle> getVehiclesWithEndLocation(@Define("startState") String state, @Define("status") String status, @Define("capacity") float capacity, @Define("endState") String endState);

    @SqlUpdate("UPDATE vehicles SET is_linked_to_user = 0 WHERE vehicle_number = '<vehicleNumber>' AND user_id = <userId> AND is_linked_to_user = 1")
    void removeVehicle(@Define("vehicleNumber") String vehicleNumber, @Define("userId") int userId);

    @SqlQuery("Select * FROM vehicles Where upper(vehicle_number) = '<vehicleNumber>' AND user_id = <userId> AND is_linked_to_user = 1")
    int isVehicleAdded(@Define("vehicleNumber") String vehicleNumber, @Define("userId") int userId);

    @SqlUpdate("UPDATE vehicles SET company_id = NULL WHERE company_id = <companyId> AND is_linked_to_user = 1")
    void removeCompanyVehicles(@Define("companyId") int companyId);
}