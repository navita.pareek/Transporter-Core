package com.transporter.core.daos;

import com.transporter.core.mapper.ConstantsMapper;
import com.transporter.core.mapper.StateCityMapper;
import com.transporter.core.models.Constants;
import com.transporter.core.models.StateCity;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.Define;
import org.skife.jdbi.v2.sqlobject.customizers.Mapper;
import org.skife.jdbi.v2.sqlobject.stringtemplate.UseStringTemplate3StatementLocator;

import java.util.List;

@UseStringTemplate3StatementLocator
public interface SupportDao {

    @SqlQuery("Select constant_key, <language> FROM constants")
    @Mapper(ConstantsMapper.class)
    List<Constants> getPreferredLanguageConstants(@Define("language") String preferredLanguage);

    @SqlQuery("Select * FROM <tableName>")
    @Mapper(StateCityMapper.class)
    List<StateCity> getAllStatesCitiesOfGivenCountry(@Define("tableName") String tableName);

    @SqlQuery("Select <language> FROM vehicle_status order by ui_index")
    List<String> getAllVehicleStatus(@Define("language") String preferredLanguage);

    @SqlQuery("Select <language> FROM trip_status order by ui_index")
    List<String> getAllTripStatus(@Define("language") String preferredLanguage);

    @SqlUpdate("INSERT INTO feedbacks(phone_number, is_Happy, want_to_connect, is_connected, utc_date_time) VALUES('<phoneNumber>', '<isHappy>', <wantToConnect>, 0, UTC_TIMESTAMP())")
    void saveCustomerFeedBack(@Define("phoneNumber") String phoneNumber, @Define("isHappy") String isHappy, @Define("wantToConnect") boolean wantToConnect);

    @SqlQuery("Select count FROM feedbacks where phone_number = '<phoneNumber>' AND is_happy = '<isHappy>'")
    int getCustomerFeedBackCount(@Define("phoneNumber") String phoneNumber, @Define("isHappy") String isHappy);

    @SqlUpdate("Update feedbacks Set count = <count>, want_to_connect = <wantToConnect>, is_connected = 0 where phone_number = '<phoneNumber>' AND is_happy = '<isHappy>'")
    void updateCustomerFeedBack(@Define("phoneNumber") String phoneNumber, @Define("isHappy") String isHappy, @Define("count") int count, @Define("wantToConnect") boolean wantToConnect);
}
