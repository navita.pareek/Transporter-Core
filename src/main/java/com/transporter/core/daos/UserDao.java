package com.transporter.core.daos;

import com.transporter.core.mapper.UserMapper;
import com.transporter.core.models.User;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.Define;
import org.skife.jdbi.v2.sqlobject.customizers.Mapper;
import org.skife.jdbi.v2.sqlobject.stringtemplate.UseStringTemplate3StatementLocator;

import javax.jws.soap.SOAPBinding;
import java.util.List;

@UseStringTemplate3StatementLocator
public interface UserDao {

    @SqlUpdate("UPDATE users SET language = '<language>' WHERE id = <userId>")
    void savePreferredLanguage(@Define("userId") Integer userId, @Define("language") String language);

    @SqlUpdate("INSERT INTO users(phone_number, date_of_birth, password, language) VALUES('<phoneNumber>', '<dateOfBirth>', '<password>', '<language>')")
    void registerUser(@Define("phoneNumber") String phoneNumber, @Define("dateOfBirth") String dateOfBirth, @Define("password") String password, @Define("language") String language);

    @SqlQuery("Select Count(*) FROM users WHERE phone_number = '<phoneNumber>'")
    int isUserRegistered(@Define("phoneNumber") String phoneNumber);

    @SqlQuery("Select id FROM users WHERE phone_number = <phoneNumber>")
    int getUserIdFromPhoneNumber(@Define("phoneNumber") String phoneNumber);

    @SqlUpdate("UPDATE users SET company_id = <company_id>, is_empower_for_structure_change = 1, is_linked_to_company = 1  WHERE phone_number = '<phoneNumber>'")
    void addCompany(@Define("phoneNumber") String phoneNumber, @Define("company_id") int companyId);

    @SqlQuery("Select phone_number FROM users WHERE company_id = <company_id> AND is_empower_for_structure_change = 1 AND is_linked_to_company = 1")
    List<String> getContactNumberForAddManpower(@Define("company_id") int companyId);

    @SqlQuery("Select Count(*) FROM users WHERE phone_number = '<phoneNumber>' AND  company_id IS NOT NULL AND is_empower_for_structure_change = 1 AND is_linked_to_company = 1")
    int isUserEligibleToAddManpower(@Define("phoneNumber") String phoneNumber);

    @SqlQuery("Select company_id FROM users WHERE phone_number = '<phoneNumber>' AND is_linked_to_company = 1")
    int getUserCompanyId(@Define("phoneNumber") String phoneNumber);

    @SqlUpdate("UPDATE users SET company_id = <company_id>, is_empower_for_structure_change = <isEmpowerForStructureChange>, is_linked_to_company = 1 WHERE phone_number = '<phoneNumber>'")
    void addManpower(@Define("phoneNumber") String newManpowerPhoneNumber, @Define("company_id") int companyId, @Define("isEmpowerForStructureChange") String isEmpowerForStructureChange);

    @SqlQuery("Select Count(*) FROM users WHERE phone_number = '<phoneNumber>' AND  company_id IS NOT NULL AND is_linked_to_company = 1")
    int isUserLinkedToAnyCompany(@Define("phoneNumber") String newManpowerPhoneNumber);

    @SqlQuery("Select Count(*) FROM users WHERE id = <userId> AND is_empower_for_structure_change = 1 AND is_linked_to_company = 1")
    int isUserEmpowered(@Define("userId") int userId);

    @SqlQuery("Select * FROM users WHERE id = <userId>")
    @Mapper(UserMapper.class)
    User getUserById(@Define("userId")  int userId);

    @SqlQuery("Select * FROM users WHERE company_id = <companyId> AND is_linked_to_company = 1")
    @Mapper(UserMapper.class)
    List<User> getUsersByCompanyId(@Define("companyId") int companyId);

    @SqlQuery("Select Count(*) FROM users WHERE phone_number = '<phoneNumber>' AND  password = '<password>'")
    int countUserPassword(@Define("phoneNumber") String phoneNumber, @Define("password") String password);

    @SqlQuery("Select language FROM users WHERE phone_number = <phoneNumber>")
    String getUserPreferredLanguage(@Define("phoneNumber") String phonenumber);

    @SqlUpdate("UPDATE users SET is_linked_to_company = 0 WHERE phone_number = '<phoneNumber>' AND company_id = <companyId>")
    void removeManpower(@Define("phoneNumber") String toRemoveManpowerPhoneNumber, @Define("companyId") int companyId);

    @SqlUpdate("UPDATE users SET is_linked_to_company = 0, company_id = NULL WHERE phone_number = '<phoneNumber>'")
    void removeCompany(@Define("phoneNumber") String phoneNumber);

    @SqlQuery("Select is_empower_for_structure_change FROM users WHERE phone_number = <phoneNumber>")
    int getIsEmpoweredToAddMembers(@Define("phoneNumber") String phoneNumber);

    @SqlQuery("Select Count(*) FROM users WHERE company_id = <company_id> AND is_empower_for_structure_change = 1 AND is_linked_to_company = 1")
    int getUsersCountWorkingForCompany(@Define("company_id") int companyId);

    @SqlUpdate("UPDATE users SET is_linked_to_company = 0, company_id = NULL WHERE company_id = <company_id>")
    void removeCompanyUsers(@Define("company_id") int companyId);

    @SqlUpdate("UPDATE users SET password = '<password>' WHERE id = <userId>")
    void changePassword(@Define("userId") int userId, @Define("password") String password);

    @SqlUpdate("UPDATE users SET user_tn_points = user_tn_points + <points> WHERE phone_number = '<phoneNumber>'")
    void addToTNPoints(@Define("phoneNumber") String phoneNumber, @Define("points") int referencePoints);

    @SqlQuery("Select user_tn_points FROM users WHERE phone_number = <phoneNumber>")
    int getUserTnPoints(@Define("phoneNumber") String phoneNumber);

    @SqlUpdate("UPDATE users SET user_contacted_count = user_contacted_count + 1 WHERE phone_number = '<phoneNumber>'")
    void updateUserContactCount(@Define("phoneNumber") String phoneNumber);
}
