package com.transporter.core.daos;

import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.Define;
import org.skife.jdbi.v2.sqlobject.stringtemplate.UseStringTemplate3StatementLocator;

@UseStringTemplate3StatementLocator
public interface CompanyDao {

    @SqlQuery("Select Count(*) FROM companies WHERE upper(name) = '<companyName>'")
    int isCompanyRegistered(@Define("companyName") String companyName);

    @SqlUpdate("INSERT INTO companies(name) VALUES('<name>')")
    void addCompany(@Define("name") String name);

    @SqlQuery("Select id FROM companies WHERE name = '<name>'")
    int getCompanyIdFromName(@Define("name") String name);

    @SqlQuery("Select name FROM companies WHERE id = '<companyId>'")
    String getCompanyNameFromId(@Define("companyId") int companyId);

    @SqlUpdate("UPDATE vehicles SET company_id = <companyId> WHERE user_id = <userId> AND company_id IS NULL")
    void includeUserVehiclesInCompany(@Define("userId") int userId, @Define("companyId") int companyId);

    @SqlUpdate("UPDATE companies SET name = '<newCompanyName>' WHERE id = <companyId>")
    void updateCompany(@Define("companyId") int companyId, @Define("newCompanyName") String newCompanyName);

    @SqlUpdate("DELETE FROM companies WHERE id = <companyId>")
    void removeCompany(@Define("companyId") int companyId);
}
