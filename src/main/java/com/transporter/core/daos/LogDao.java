package com.transporter.core.daos;

import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.Define;
import org.skife.jdbi.v2.sqlobject.stringtemplate.UseStringTemplate3StatementLocator;

@UseStringTemplate3StatementLocator
public interface LogDao {

    @SqlUpdate("INSERT INTO logs(class, log_level, message, method, phone_number, request, response, response_size, date_time) VALUES('<className>', '<logLevel>', '<message>', '<method>', '<phoneNumber>', '<request>', '<response>', '<responseSize>', UTC_TIMESTAMP())")
    void log(@Define("className") String className, @Define("logLevel") String logLevel, @Define("message") String message, @Define("method")  String method, @Define("phoneNumber") String phoneNumber, @Define("request") String request, @Define("response") String response, @Define("responseSize") int responseSize);
}
