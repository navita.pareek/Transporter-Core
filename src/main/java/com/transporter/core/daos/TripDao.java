package com.transporter.core.daos;

import com.transporter.core.mapper.TripMapper;
import com.transporter.core.models.Trip;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.Define;
import org.skife.jdbi.v2.sqlobject.customizers.Mapper;
import org.skife.jdbi.v2.sqlobject.stringtemplate.UseStringTemplate3StatementLocator;

import java.util.List;

@UseStringTemplate3StatementLocator
public interface TripDao {

    @SqlUpdate("INSERT INTO trips(start_date, start_location, end_location, vehicle_capacity_required, status, description, user_id)  VALUES('<startDate>', '<startLocation>', '<endLocation>', <vehicleCapacityRequired>, '<tripStatus>', '<tripDescription>', <userId>)")
    void addTrip(@Define("startDate") String startDate, @Define("startLocation") String startLocation, @Define("endLocation") String endLocation, @Define("vehicleCapacityRequired") float vehicleCapacityRequired, @Define("tripStatus") String tripStatus, @Define("tripDescription") String tripDescription, @Define("userId") int userId);

    @SqlQuery("Select Count(*) from trips where start_date = '<startDate>' AND start_location = '<startLocation>' AND end_location = '<endLocation>' AND user_id = <userId> AND 0.0001 >= ABS(vehicle_capacity_required - <vehicleCapacityRequired>)")
    int isTripExists(@Define("startDate") String startDate, @Define("startLocation") String startLocation, @Define("endLocation") String endLocation, @Define("vehicleCapacityRequired") float vehicleCapacityRequired, @Define("userId") int userId);

    @SqlQuery("Select * from trips where start_date >= '<startDate>' AND start_location Like '<startState>' AND end_location Like '<endState>' AND status = '<status>' Order by vehicle_capacity_required")
    @Mapper(TripMapper.class)
    List<Trip> searchTripWithEndLocation(@Define("startDate") String startDate, @Define("startState") String startState, @Define("status") String status, @Define("endState") String endState);


    @SqlQuery("Select * from trips")
    @Mapper(TripMapper.class)
    List<Trip> getAllTrips();


    @SqlQuery("Select * from trips where user_id = <userId> AND status != '<status>' AND status != '<status2>'")
    @Mapper(TripMapper.class)
    List<Trip> getTripsByUserIdNotInStatus(@Define("userId") int userId, @Define("status") String status, @Define("status2") String status2);

    @SqlUpdate("Update trips Set status = '<newTripStatus>' where start_date = '<startDate>' AND start_location = '<startLocation>' AND end_location = '<endLocation>' AND 0.0001 >= ABS(vehicle_capacity_required - <vehicleCapacityRequired>) AND user_id = <userId> AND status = '<tripStatus>'")
    void updateTrip(@Define("startDate") String startDate, @Define("startLocation") String startLocation, @Define("endLocation") String endLocation, @Define("vehicleCapacityRequired") float vehicleCapacityRequired, @Define("tripStatus") String tripStatus, @Define("userId") int userId, @Define("newTripStatus") String newTripStatus);
}
