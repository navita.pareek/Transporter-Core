package com.transporter.core.services;

import com.transporter.core.constants.StringConstants;
import com.transporter.core.daos.UserDao;
import com.transporter.core.daos.VehicleDao;
import com.transporter.core.models.Vehicle;
import com.transporter.core.tools.ListToStringConverter;
import com.transporter.core.tools.StringToJsonSerializedString;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class VehicleService {

    @Autowired
    private VehicleDao vehicleDao;

    @Autowired
    private UserDao userDao;

    public boolean isVehicleAdded(String vehicleNumber) {
        vehicleNumber = removeExtraSpaces(vehicleNumber.trim());
        if(vehicleDao.isVehicleRegistered(vehicleNumber.toUpperCase()) > 0)
            return true;
        return false;
    }

    public String addVehicle(Vehicle vehicle, String phoneNumber) {
        int userId = userDao.getUserIdFromPhoneNumber(phoneNumber);

        if(userId < 1)
            return StringConstants.USER_NOT_REGISTERED;

        if(vehicle.getIsLinkedToCompany().equals("true")){
            int companyId = userDao.getUserCompanyId(phoneNumber);
            if(companyId < 1) {
                vehicleDao.addVehicle(userId, removeExtraSpaces(vehicle.getVehicleNumber().trim()), vehicle.getCapacity(), vehicle.getStartLocation(), vehicle.getEndLocation(), vehicle.getStatus());
            }else {
                vehicle.setCompanyId(companyId);
                vehicleDao.addVehicle(userId, removeExtraSpaces(vehicle.getVehicleNumber().trim()), vehicle.getCapacity(), vehicle.getStartLocation(), vehicle.getEndLocation(), vehicle.getStatus(), vehicle.getCompanyId());
            }
        }else{
            vehicleDao.addVehicle(userId, removeExtraSpaces(vehicle.getVehicleNumber().trim()), vehicle.getCapacity(), vehicle.getStartLocation(), vehicle.getEndLocation(), vehicle.getStatus());
        }
        return StringConstants.SUCCESS;
    }

    public String getUserVehicles(int userId, int companyId) {
        List<Vehicle> vehicles = new ArrayList<>();

        vehicles.addAll(vehicleDao.getUserVehiclesByUserIdNotLinkedToCompany(userId));

        if((companyId > 0)){
            vehicles.addAll(vehicleDao.getUserVehiclesByCompanyId(companyId));
        }

        String result = ListToStringConverter.getInstance().convert(vehicles);
        result = StringToJsonSerializedString.ConvertToJsonSerializedString(result, StringConstants.vehicles);
        return result;
    }

    public String updateVehicle(Vehicle vehicle) {
        vehicleDao.updateVehicle(vehicle.getVehicleNumber(), vehicle.getStartLocation(), vehicle.getEndLocation(), vehicle.getStatus(), vehicle.getCapacity());
        return StringConstants.SUCCESS;
    }

    public String removeVehicle(String vehicleNumber, int userId) {
        vehicleDao.removeVehicle(vehicleNumber, userId);
        return StringConstants.SUCCESS;
    }

    public boolean isVehicleAdded(String vehicleNumber, int userId) {
        vehicleNumber = removeExtraSpaces(vehicleNumber.trim());
        if(vehicleDao.isVehicleAdded(vehicleNumber.toUpperCase(), userId) > 0)
             return true;

         return false;
    }

    private String removeExtraSpaces(String name) {
        while(true){
            name = name.replace("  ", " ");
            if(!name.contains("  ")){
                return name;
            }
        }
    }

    public void removeCompanyVehicles(int companyId) {
        vehicleDao.removeCompanyVehicles(companyId);
    }
}
