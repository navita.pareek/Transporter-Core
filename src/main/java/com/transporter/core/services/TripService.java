package com.transporter.core.services;

import com.transporter.core.constants.StringConstants;
import com.transporter.core.daos.CompanyDao;
import com.transporter.core.daos.TripDao;
import com.transporter.core.daos.UserDao;
import com.transporter.core.daos.VehicleDao;
import com.transporter.core.models.Trip;
import com.transporter.core.models.User;
import com.transporter.core.models.Vehicle;
import com.transporter.core.tools.ListToStringConverter;
import com.transporter.core.tools.StringToJsonSerializedString;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
public class TripService {

    @Autowired
    private TripDao tripDao;

    @Autowired
    private VehicleDao vehicleDao;

    @Autowired
    private CompanyDao companyDao;

    @Autowired
    private UserDao userDao;

    public String addTrip(Trip trip, int userId) {
        tripDao.addTrip(trip.getStartDate(), trip.getStartStateIndex() + ':' + trip.getStartCityIndex(),
                trip.getEndStateIndex() + ':' + trip.getEndCityIndex(), trip.getVehicleCapacityRequired(),
        trip.getTripStatus(), trip.getTripDescription(), userId);
        return StringConstants.SUCCESS;
    }

    public String getMatchedVehiclesContactNumber(Trip trip) {
        Set<Vehicle> vehicles = new HashSet<>();

        String startLocation = trip.getStartStateIndex() + ':' + trip.getStartCityIndex();
        String endLocation = trip.getEndStateIndex() + ':' + trip.getEndCityIndex();
        vehicles.addAll(getAvailableOrUnloadingVehiclesInStateWithEndState(startLocation, trip.getVehicleCapacityRequired(), endLocation));

        if(vehicles.size() < StringConstants.minContacts){
            endLocation = trip.getEndStateIndex() + ":%";
            vehicles.addAll(getAvailableOrUnloadingVehiclesInStateWithEndState(startLocation, trip.getVehicleCapacityRequired(), endLocation));
        }

        if(vehicles.size() < StringConstants.minContacts){
            startLocation = trip.getStartStateIndex() + ":%";
            vehicles.addAll(getAvailableOrUnloadingVehiclesInStateWithEndState(startLocation, trip.getVehicleCapacityRequired(), endLocation));
        }

        if(vehicles.size() < StringConstants.minContacts){
            endLocation = "0:0";
            vehicles.addAll(getAvailableOrUnloadingVehiclesInStateWithEndState(startLocation, trip.getVehicleCapacityRequired(), endLocation));
        }

        if(vehicles.size() < StringConstants.minContacts){
            startLocation = "0:0";
            vehicles.addAll(getAvailableOrUnloadingVehiclesInStateWithEndState(startLocation, trip.getVehicleCapacityRequired(), endLocation));
        }

        if(vehicles.size() < StringConstants.minContacts){
            endLocation = trip.getEndStateIndex() + ':' + trip.getEndCityIndex();
            vehicles.addAll(getAvailableOrUnloadingVehiclesInStateWithEndState(startLocation, trip.getVehicleCapacityRequired(), endLocation));
        }

        if(vehicles.size() < StringConstants.minContacts){
            endLocation = trip.getEndStateIndex() + ":0";
            vehicles.addAll(getAvailableOrUnloadingVehiclesInStateWithEndState(startLocation, trip.getVehicleCapacityRequired(), endLocation));
        }

        List<User> vehicleOwners = getOwnerContactNumber(new ArrayList<>(vehicles));
        addUserListWithCompanyName(vehicleOwners);
        orderUserListAsPerTnPoints(vehicleOwners);

        String result = ListToStringConverter.getInstance().convert(vehicleOwners);
        result = StringToJsonSerializedString.ConvertToJsonSerializedString(result, StringConstants.users);
        return result;
    }

    private void orderUserListAsPerTnPoints(List<User> vehicleOwners) {
        Collections.sort(vehicleOwners, (s1, s2) ->
                Integer.compare(s2.getUserTnPoints(), s1.getUserTnPoints()));
    }

    private void addUserListWithCompanyName(List<User> vehicleOwners) {
        for (int i = 0; i < vehicleOwners.size(); i++) {
            int companyId = userDao.getUserCompanyId(vehicleOwners.get(i).getPhoneNumber());
            String companyName = companyDao.getCompanyNameFromId(companyId);
            if(companyName != null && companyName.length() != 0) {
                vehicleOwners.get(i).setCompanyName(companyName);
            }else{
                vehicleOwners.get(i).setCompanyName(" ------ ");
            }
        }
    }

    private String replaceZeroAsIndex(String location) {
        if(location.indexOf(":0") > 0)
            location = location.replace(":0", ":%");

        if(location.indexOf("0:") == 0)
            location = location.replace("0:", "%:");

        return location;
    }

    private List<Vehicle> getAvailableOrUnloadingVehiclesInStateWithEndState(String startLocation, float vehicleCapacityRequired, String endLocation) {
        List<Vehicle> vehicles = vehicleDao.getVehiclesWithEndLocation(startLocation, StringConstants.vehicle_available_for_booking, vehicleCapacityRequired, endLocation);
        vehicles.addAll(vehicleDao.getVehiclesWithEndLocation(startLocation, StringConstants.vehicle_unloading, vehicleCapacityRequired, endLocation));
        return vehicles;
    }

    private List<User> getOwnerContactNumber(List<Vehicle> vehicles) {
        List<User> users = new ArrayList<>();
        for(int i = 0; i < vehicles.size(); i++){
            if(vehicles.get(i).getCompanyId() > 0){
                users.addAll(userDao.getUsersByCompanyId(vehicles.get(i).getCompanyId()));
            }else {
                User user = userDao.getUserById(vehicles.get(i).getUserId());
                users.add(user);
            }
        }
        return users;
    }

    public boolean isTripExists(Trip trip, int userId) {
        return tripDao.isTripExists(trip.getStartDate(), trip.getStartStateIndex() + ':' + trip.getStartCityIndex(),
                trip.getEndStateIndex() + ':' + trip.getEndCityIndex(), trip.getVehicleCapacityRequired(), userId) > 0;
    }

    public String searchTrip(Trip trip) {
        List<Trip> trips = filterTripsByCapacity(getTrips(trip), trip.getVehicleCapacityRequired());
        if(trips == null || trips.size() == 0){
            trips = tripDao.getAllTrips();
        }

        List<User> users = getUsersRelatedToTrips(trips);
        addUserListWithCompanyName(users);
        orderUserListAsPerTnPoints(users);

        String result = ListToStringConverter.getInstance().convert(users);
        result = StringToJsonSerializedString.ConvertToJsonSerializedString(result, StringConstants.users);
        return result;
    }

    private List<Trip> filterTripsByCapacity(List<Trip> trips, float vehicleCapacityRequired) {
        List<Trip> result = new ArrayList<>();
        for(int i = 0; i < trips.size(); i++){
             if(trips.get(i).getVehicleCapacityRequired() <= vehicleCapacityRequired) {
                result.add(trips.get(i));
             }
        }
        return result;
    }

    private List<User> getUsersRelatedToTrips(List<Trip> trips) {
        List<User> users = new ArrayList<>();
        for(int i = 0; i < trips.size(); i++){
            users.add(userDao.getUserById(trips.get(i).getUserId()));
        }
        return users;
    }

    private List<Trip> getTrips(Trip trip) {
        List<Trip> trips = new LinkedList<>();

        String startLocation = trip.getStartStateIndex() + ':' + trip.getStartCityIndex();
        String endLocation = trip.getEndStateIndex() + ':' + trip.getEndCityIndex();
        startLocation = replaceZeroAsIndex(startLocation);
        endLocation = replaceZeroAsIndex(endLocation);

        trips.addAll(tripDao.searchTripWithEndLocation(trip.getStartDate(), startLocation, trip.getTripStatus(), endLocation));
        return trips;
    }

    public String getTripsByUserId(int userId) {
        List<Trip> trips = new ArrayList<>();
        trips.addAll(tripDao.getTripsByUserIdNotInStatus(userId, StringConstants.trip_finished, StringConstants.trip_cancelled));
        String result = ListToStringConverter.getInstance().convert(trips);
        result = StringToJsonSerializedString.ConvertToJsonSerializedString(result, StringConstants.trips);
        return result;
    }

    public String updateTrip(Trip trip, int userId, String newTripStatus) {
        tripDao.updateTrip(trip.getStartDate(),trip.getStartStateIndex() + ':' + trip.getStartCityIndex(),
                trip.getEndStateIndex() + ':' + trip.getEndCityIndex(), trip.getVehicleCapacityRequired(),
        trip.getTripStatus(), userId, newTripStatus);
        return StringConstants.SUCCESS;
    }
}
