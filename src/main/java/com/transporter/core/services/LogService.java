package com.transporter.core.services;

import com.transporter.core.daos.LogDao;

import com.transporter.core.models.LogEntry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.logging.Level;
import java.util.logging.Logger;

@Component
@Scope(value = "singleton")
public class LogService {

    Logger logger = Logger.getLogger(LogService.class.getName());

    @Autowired
    private LogDao logDao;

    public void log(LogEntry logEntry) {
        try{
            logDao.log(logEntry.getClassName(), logEntry.getLogLevel(), logEntry.getMessage(), logEntry.getMethod(), logEntry.getPhoneNumber(), logEntry.getRequest(), logEntry.getResponse(), logEntry.getResponseSize());
        }catch(Exception ex){
            logger.log(Level.SEVERE, "Unable to log: " + logEntry.toString());
        }
    }
}
