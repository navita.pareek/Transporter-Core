package com.transporter.core.services;

import com.transporter.core.constants.StringConstants;
import com.transporter.core.daos.SupportDao;
import com.transporter.core.models.Constants;
import com.transporter.core.models.CustomerFeedback;
import com.transporter.core.models.StateCity;
import com.transporter.core.tools.ListToStringConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import java.util.List;

@Component
public class SupportService {

    @Autowired
    private SupportDao supportDao;

    public String getPreferredLanguageConstants(String preferredLanguage) {
        List<Constants> languageConstants = supportDao.getPreferredLanguageConstants(preferredLanguage);
        String result = ListToStringConverter.getInstance().convert(languageConstants);
        return result;
    }

    public String getAllStatesCitiesOfGivenCountry(String preferredLanguage) {
        String tableName = getStateCityTableName(preferredLanguage);
        List<StateCity> languageConstants = supportDao.getAllStatesCitiesOfGivenCountry(tableName);
        String result = ListToStringConverter.getInstance().convert(languageConstants);
        return result;
    }

    private String getStateCityTableName(String preferredLanguage) {
        if(preferredLanguage.equals("HINDI"))
            return "india_states_cities_hindi";

        return "india_states_cities";
    }

    public String getAllVehicleStatus(String preferredLanguage) {
        List<String> vehicleStatus = supportDao.getAllVehicleStatus(preferredLanguage);
        String result = ListToStringConverter.getInstance().convert(vehicleStatus);
        return result;
    }

    public String getAllTripsStatus(String preferredLanguage) {
        List<String> tripStatus = supportDao.getAllTripStatus(preferredLanguage);
        String result = ListToStringConverter.getInstance().convert(tripStatus);
        return result;
    }

    public String saveCustomerFeedBack(CustomerFeedback input) {
        int count = supportDao.getCustomerFeedBackCount(input.getPhoneNumber(), input.getIsHappy());
        if(count > 0){
            supportDao.updateCustomerFeedBack(input.getPhoneNumber(), input.getIsHappy(), count+1, input.getWantToConnect());
        }else {
            supportDao.saveCustomerFeedBack(input.getPhoneNumber(), input.getIsHappy(), input.getWantToConnect());
        }
        return StringConstants.SUCCESS;
    }
}
