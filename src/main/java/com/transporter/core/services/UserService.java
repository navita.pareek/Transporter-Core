package com.transporter.core.services;

import com.transporter.core.constants.StringConstants;
import com.transporter.core.daos.CompanyDao;
import com.transporter.core.daos.UserDao;
import com.transporter.core.models.Company;
import com.transporter.core.models.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class UserService {

    @Autowired
    private UserDao userDao;

    @Autowired
    private CompanyDao companyDao;

    public String savePreferredLanguage(Integer userId, String preferredLanguage) {
        userDao.savePreferredLanguage(userId, preferredLanguage);
        return StringConstants.SUCCESS;
    }

    public boolean isUserRegistered(String phoneNumber) {
        if(userDao.isUserRegistered(phoneNumber) > 0)
            return true;
        return false;
    }

    public String registerUser(User user) {
        userDao.registerUser(user.getPhoneNumber(), user.getDateOfBirth(), user.getPassword(), user.getLanguage());
        return StringConstants.SUCCESS;
    }

    public String addCompany(Company company, String phoneNumber) {
        int companyId = companyDao.getCompanyIdFromName(company.getName());
        if(companyId < 1)
            return StringConstants.COMPANY_NOT_REGISTERED;

        userDao.addCompany(phoneNumber, companyId);
        return StringConstants.SUCCESS;
    }

    public String getContactNumberToAddManpower(int companyId) {
        return String.join(", ",userDao.getContactNumberForAddManpower(companyId));
    }

    public boolean isUserEligibleToAddManpower(String phoneNumber) {
        return (userDao.isUserEligibleToAddManpower(phoneNumber) > 0);
    }

    public int getUserCompanyId(String phoneNumber) {
        return userDao.getUserCompanyId(phoneNumber);
    }

    public String addManpower(String newManpowerPhoneNumber, int companyId, String isEmpowerForStructureChange) {
        userDao.addManpower(newManpowerPhoneNumber, companyId, isEmpowerForStructureChange);
        return StringConstants.SUCCESS;
    }

    public boolean isUserLinkedToAnyCompany(String newManpowerPhoneNumber) {
        if(userDao.isUserLinkedToAnyCompany(newManpowerPhoneNumber) > 0)
            return false;

        return true;
    }

    public int getUserId(String phoneNumber) {
        return userDao.getUserIdFromPhoneNumber(phoneNumber);
    }

    public String isValidUserPassword(User user) {
        if(userDao.countUserPassword(user.getPhoneNumber(), user.getPassword()) > 0){
            return StringConstants.SUCCESS;
        }
        return StringConstants.INVALID_INFORMATION_PROVIDED;
    }

    public String getUserPreferredLanguage(String phonenumber) {
        return userDao.getUserPreferredLanguage(phonenumber);
    }

    public String getUserDetails(int userId) {
        return (userDao.getUserById(userId)).toString();
    }

    public String removeManpower(String toRemoveManpowerPhoneNumber, int companyId) {
        userDao.removeManpower(toRemoveManpowerPhoneNumber, companyId);
        return StringConstants.SUCCESS;
    }

    public String removeUserCompany(String phoneNumber) {
        userDao.removeCompany(phoneNumber);
        return StringConstants.SUCCESS;
    }

    public String getIsEmpoweredToAddMembers(String phoneNumber) {
        return userDao.getIsEmpoweredToAddMembers(phoneNumber) == 1 ? "true" : "false";
    }

    public List<User> getCompanyMembers(int companyId) {
        return userDao.getUsersByCompanyId(companyId);
    }

    public int getUsersCountWorkingForCompany(int companyId) {
        return userDao.getUsersCountWorkingForCompany(companyId);
    }

    public void removeCompanyUsers(int companyId) {
        userDao.removeCompanyUsers(companyId);
    }

    public String changePassword(int userId, String password) {
        userDao.changePassword(userId, password);
        return StringConstants.SUCCESS;
    }

    public void addToTNPoints(String phoneNumber, int referencePoints) {
        userDao.addToTNPoints(phoneNumber, referencePoints);
    }

    public int getUserTnPoints(String phoneNumber) {
        return userDao.getUserTnPoints(phoneNumber);
    }

    public void updateUserContactCount(User user) {
        userDao.updateUserContactCount(user.getReferencePhoneNumber());
    }
}
