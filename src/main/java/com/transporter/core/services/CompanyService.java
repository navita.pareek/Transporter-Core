package com.transporter.core.services;

import com.transporter.core.constants.StringConstants;
import com.transporter.core.daos.CompanyDao;
import com.transporter.core.models.Company;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CompanyService {

    @Autowired
    private CompanyDao companyDao;

    public boolean isCompanyRegistered(String companyName) {
        companyName = removeExtraSpaces(companyName.trim());
        if(companyDao.isCompanyRegistered(companyName.toUpperCase()) > 0)
                return true;
        return false;
    }

    public String addCompany(Company company) {
        String companyName = removeExtraSpaces(company.getName().trim());
        companyDao.addCompany(companyName);
        return StringConstants.SUCCESS;
    }

    public int getCompanyIdFromName(String companyName) {
        return companyDao.getCompanyIdFromName(companyName);
    }

    public String getCompanyNameFromId(int companyId) {
        return companyDao.getCompanyNameFromId(companyId);
    }

    public String includeUserVehiclesInCompany(int userId, int companyId) {
        companyDao.includeUserVehiclesInCompany(userId, companyId);
        return StringConstants.SUCCESS;
    }

    public void updateCompany(int companyId, String newCompanyName) {
        newCompanyName = removeExtraSpaces(newCompanyName.trim());
        companyDao.updateCompany(companyId, newCompanyName);
    }

    private String removeExtraSpaces(String name) {
        while(true){
            name = name.replace("  ", " ");
            if(!name.contains("  ")){
                return name;
            }
        }
    }

    public void removeCompany(int companyId) {
        companyDao.removeCompany(companyId);
    }
}
