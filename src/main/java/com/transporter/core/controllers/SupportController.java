package com.transporter.core.controllers;

import com.transporter.core.constants.StringConstants;
import com.transporter.core.models.CustomerFeedback;
import com.transporter.core.services.SupportService;
import com.transporter.core.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("/support")
public class SupportController {

    @Autowired
    private SupportService supportService;

    @Autowired
    private UserService userService;

    @RequestMapping(value = "string_constants/{language}", method = RequestMethod.GET)
    public String getPreferredLanguageConstants(@PathVariable("language") String language){

        if(language == null){
            return StringConstants.REQUIRED_INPUTS_ARE_NOT_PROVIDED;
        }
        return supportService.getPreferredLanguageConstants(language);
    }

    @RequestMapping(value = "states/cities/{phone_number}", method = RequestMethod.GET)
    public String getAllStatesCitiesOfGivenCountry(@PathVariable("phone_number") String phoneNumber){

        if(!userService.isUserRegistered(phoneNumber))
            return StringConstants.USER_NOT_REGISTERED;

        String preferredLanguage = userService.getUserPreferredLanguage(phoneNumber);
        return supportService.getAllStatesCitiesOfGivenCountry(preferredLanguage);
    }

    @RequestMapping(value = "vehicle_status/{phone_number}", method = RequestMethod.GET)
    public String getAllVehicleStatus(@PathVariable("phone_number") String phoneNumber){

        if(!userService.isUserRegistered(phoneNumber))
            return StringConstants.USER_NOT_REGISTERED;

        String preferredLanguage = userService.getUserPreferredLanguage(phoneNumber);
        return supportService.getAllVehicleStatus(preferredLanguage);
    }

    @RequestMapping(value = "trip_status/{phone_number}", method = RequestMethod.GET)
    public String getAllTripsStatus(@PathVariable("phone_number") String phoneNumber){

        if(!userService.isUserRegistered(phoneNumber))
            return StringConstants.USER_NOT_REGISTERED;

        String preferredLanguage = userService.getUserPreferredLanguage(phoneNumber);
        return supportService.getAllTripsStatus(preferredLanguage);
    }

    @RequestMapping(value = "feedback", method = RequestMethod.POST)
    public String saveCustomerFeedBack(@RequestBody Map<String, String> payload){

        String phoneNumber = payload.get(StringConstants.phoneNumber);
        String isHappy = payload.get(StringConstants.isHappy);

        if(!isHappy.equals(StringConstants.NEW_CUSTOMER_SUPPORT) && !userService.isUserRegistered(phoneNumber))
            return StringConstants.USER_NOT_REGISTERED;

        boolean wantToConnect = payload.get(StringConstants.wantToConnect).equals("true") ? true : false;

        CustomerFeedback input = new CustomerFeedback(phoneNumber, isHappy);
        input.setWantToConnect(wantToConnect);

        return supportService.saveCustomerFeedBack(input);
    }

}
