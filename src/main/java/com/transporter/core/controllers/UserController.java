package com.transporter.core.controllers;

import com.transporter.core.constants.StringConstants;
import com.transporter.core.models.LogEntry;
import com.transporter.core.models.User;
import com.transporter.core.services.LogService;
import com.transporter.core.services.UserService;

import com.transporter.core.services.VehicleService;
import com.transporter.core.tools.StringMagician;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;
import java.util.logging.Level;

@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private VehicleService vehicleService;

    @Autowired
    private LogService logService;

    private  final String CLASS_NAME = "com.transporter.core.controllers.UserController";
    
    @RequestMapping(value = "language", method = RequestMethod.POST)
    public String savePreferredLanguage(@RequestBody Map<String, String> payload){

        String phoneNumber = payload.get(StringConstants.phoneNumber);
        LogEntry logEntry = new LogEntry(CLASS_NAME, Level.INFO.toString(), "ENTRY", "savePreferredLanguage", payload.toString(), "TODO", phoneNumber, -1);
        logService.log(logEntry);

        try{
            if(!userService.isUserRegistered(phoneNumber)){
                logEntry = new LogEntry(CLASS_NAME, Level.INFO.toString(), "INVALID_INPUT", "savePreferredLanguage", payload.toString(), StringConstants.USER_NOT_REGISTERED, phoneNumber, -1);
                logService.log(logEntry);
                return StringConstants.USER_NOT_REGISTERED;
            }

            String language = payload.get(StringConstants.language);
            if(language == null || language.trim().length() == 0){
                logEntry = new LogEntry(CLASS_NAME, Level.INFO.toString(), "INVALID_INPUT", "savePreferredLanguage", payload.toString(), StringConstants.REQUIRED_INPUTS_ARE_NOT_PROVIDED, phoneNumber, -1);
                logService.log(logEntry);
                return StringConstants.REQUIRED_INPUTS_ARE_NOT_PROVIDED;
            }

            int userId = userService.getUserId(phoneNumber);
            String response = userService.savePreferredLanguage(userId, language);
            logEntry = new LogEntry(CLASS_NAME, Level.INFO.toString(), "EXIT", "savePreferredLanguage", payload.toString(), response, phoneNumber, response.length());
            logService.log(logEntry);
            return response;

        }catch(Exception ex){
            logEntry = new LogEntry(CLASS_NAME, Level.INFO.toString(), "EXCEPTION", "savePreferredLanguage", payload.toString(), ex.getMessage(), phoneNumber, -1);
            logService.log(logEntry);
            return StringConstants.SERVER_UNDER_MAINTENANCE;
        }
    }

    @RequestMapping(value = "register", method = RequestMethod.POST)
    public String registerUser(@RequestBody Map<String, String> payload){

        String phoneNumber = payload.get(StringConstants.phoneNumber);
        LogEntry logEntry = new LogEntry(CLASS_NAME, Level.INFO.toString(), "ENTRY", "registerUser", payload.toString(), "TODO", phoneNumber, -1);
        logService.log(logEntry);

        try{
            if(userService.isUserRegistered(phoneNumber)){
                logEntry = new LogEntry(CLASS_NAME, Level.INFO.toString(), "INVALID_INPUT", "registerUser", payload.toString(), StringConstants.USER_ALREADY_REGISTERED, phoneNumber, -1);
                logService.log(logEntry);
                return StringConstants.USER_ALREADY_REGISTERED;
            }

            String dateOfBirth = payload.get(StringConstants.dateOfBirth);
            String password = payload.get(StringConstants.password);
            String language = payload.get(StringConstants.language);
            if(null == phoneNumber || null == dateOfBirth || null == password || (!("ENGLISH").equals(language) && !("HINDI").equals(language))) {
                logEntry = new LogEntry(CLASS_NAME, Level.INFO.toString(), "INVALID_INPUT", "registerUser", payload.toString(), StringConstants.REQUIRED_INPUTS_ARE_NOT_PROVIDED, phoneNumber, -1);
                logService.log(logEntry);
                return StringConstants.REQUIRED_INPUTS_ARE_NOT_PROVIDED;
            }

            password = StringMagician.getInstance().getEncryptedString(password);
            User user = new User(phoneNumber, dateOfBirth, password);
            if(null != language)
                user.setLanguage(language);

            String response = userService.registerUser(user);
            logEntry = new LogEntry(CLASS_NAME, Level.INFO.toString(), "EXIT", "registerUser", payload.toString(), response, phoneNumber, response.length());
            logService.log(logEntry);

            String referencePhoneNumber = payload.get(StringConstants.referencePhoneNumber);
            if(referencePhoneNumber != null && referencePhoneNumber.length() != 0 && !referencePhoneNumber.equals(phoneNumber) && userService.isUserRegistered(referencePhoneNumber)){
                userService.addToTNPoints(referencePhoneNumber, StringConstants.referencePoints);
                userService.addToTNPoints(phoneNumber, StringConstants.referencePoints);
            }

            return response;

        }catch(Exception ex){
            logEntry = new LogEntry(CLASS_NAME, Level.INFO.toString(), "EXCEPTION", "registerUser", payload.toString(), ex.getMessage(), phoneNumber, -1);
            logService.log(logEntry);
            return StringConstants.SERVER_UNDER_MAINTENANCE;
        }
    }

    @RequestMapping(value = "manpower/add", method = RequestMethod.POST)
    public String addManpower(@RequestBody Map<String, String> payload) {

        String phoneNumber = payload.get(StringConstants.phoneNumber);
        LogEntry logEntry = new LogEntry(CLASS_NAME, Level.INFO.toString(), "ENTRY", "addManpower", payload.toString(), "TODO", phoneNumber, -1);
        logService.log(logEntry);

        try{
            if(!userService.isUserRegistered(phoneNumber)){
                logEntry = new LogEntry(CLASS_NAME, Level.INFO.toString(), "INVALID_INPUT", "addManpower", payload.toString(), StringConstants.USER_NOT_REGISTERED, phoneNumber, -1);
                logService.log(logEntry);
                return StringConstants.USER_NOT_REGISTERED;
            }

            if(!userService.isUserEligibleToAddManpower(phoneNumber)){
                logEntry = new LogEntry(CLASS_NAME, Level.INFO.toString(), "INVALID_INPUT", "addManpower", payload.toString(), StringConstants.USER_NOT_ELIGIBLE_TO_ADD_MANPOWER, phoneNumber, -1);
                logService.log(logEntry);
                return StringConstants.USER_NOT_ELIGIBLE_TO_ADD_MANPOWER;
            }

            String newManpowerPhoneNumber = payload.get(StringConstants.newManpowerPhoneNumber);
            if(!userService.isUserRegistered(newManpowerPhoneNumber)){
                logEntry = new LogEntry(CLASS_NAME, Level.INFO.toString(), "INVALID_INPUT", "addManpower", payload.toString(), StringConstants.USER_NOT_REGISTERED, phoneNumber, -1);
                logService.log(logEntry);
                return StringConstants.USER_NOT_REGISTERED;
            }

            if(!userService.isUserLinkedToAnyCompany(newManpowerPhoneNumber)){
                logEntry = new LogEntry(CLASS_NAME, Level.INFO.toString(), "INVALID_INPUT", "addManpower", payload.toString(), StringConstants.USER_IS_ALREADY_WORKING_FOR_SOME_COMPANY, phoneNumber, -1);
                logService.log(logEntry);
                return StringConstants.USER_IS_ALREADY_WORKING_FOR_SOME_COMPANY;
            }

            String isEmpowerForStructureChange = (payload.get(StringConstants.isEmpowerForStructureChange) != null) ? payload.get(StringConstants.isEmpowerForStructureChange) : "false";
            int companyId = userService.getUserCompanyId(phoneNumber);
            String response = userService.addManpower(newManpowerPhoneNumber, companyId, isEmpowerForStructureChange);
            logEntry = new LogEntry(CLASS_NAME, Level.INFO.toString(), "EXIT", "addManpower", payload.toString(), response, phoneNumber, response.length());
            logService.log(logEntry);
            return response;

        }catch(Exception ex){
            logEntry = new LogEntry(CLASS_NAME, Level.INFO.toString(), "EXCEPTION", "addManpower", payload.toString(), ex.getMessage(), phoneNumber, -1);
            logService.log(logEntry);
            return StringConstants.SERVER_UNDER_MAINTENANCE;
        }
    }

    @RequestMapping(value = "manpower/remove", method = RequestMethod.POST)
    public String removeManpower(@RequestBody Map<String, String> payload) {

        String phoneNumber = payload.get(StringConstants.phoneNumber);
        LogEntry logEntry = new LogEntry(CLASS_NAME, Level.INFO.toString(), "ENTRY", "removeManpower", payload.toString(), "TODO", phoneNumber, -1);
        logService.log(logEntry);

        try{
            if(!userService.isUserRegistered(phoneNumber)){
                logEntry = new LogEntry(CLASS_NAME, Level.INFO.toString(), "INVALID_INPUT", "removeManpower", payload.toString(), StringConstants.USER_NOT_REGISTERED, phoneNumber, -1);
                logService.log(logEntry);
                return StringConstants.USER_NOT_REGISTERED;
            }

            if(!userService.isUserEligibleToAddManpower(phoneNumber)){
                logEntry = new LogEntry(CLASS_NAME, Level.INFO.toString(), "INVALID_INPUT", "removeManpower", payload.toString(), StringConstants.USER_NOT_ELIGIBLE_TO_ADD_MANPOWER, phoneNumber, -1);
                logService.log(logEntry);
                return StringConstants.USER_NOT_ELIGIBLE_TO_ADD_MANPOWER;
            }

            String toRemoveManpowerPhoneNumber = payload.get(StringConstants.newManpowerPhoneNumber);
            if(!userService.isUserRegistered(toRemoveManpowerPhoneNumber)){
                logEntry = new LogEntry(CLASS_NAME, Level.INFO.toString(), "INVALID_INPUT", "removeManpower", payload.toString(), StringConstants.USER_NOT_REGISTERED, phoneNumber, -1);
                logService.log(logEntry);
                return StringConstants.USER_NOT_REGISTERED;
            }

            int companyId = userService.getUserCompanyId(phoneNumber);
            int toRemoveUserCompanyId = userService.getUserCompanyId(toRemoveManpowerPhoneNumber);
            if(companyId != toRemoveUserCompanyId){
                logEntry = new LogEntry(CLASS_NAME, Level.INFO.toString(), "INVALID_INPUT", "removeManpower", payload.toString(), StringConstants.USER_NOT_IN_SAME_COMPANY, phoneNumber, -1);
                logService.log(logEntry);
                return StringConstants.USER_NOT_IN_SAME_COMPANY;
            }

            String response = userService.removeManpower(toRemoveManpowerPhoneNumber, companyId);
            logEntry = new LogEntry(CLASS_NAME, Level.INFO.toString(), "EXIT", "removeManpower", payload.toString(), response, phoneNumber, response.length());
            logService.log(logEntry);
            return response;

        }catch(Exception ex){
            logEntry = new LogEntry(CLASS_NAME, Level.INFO.toString(), "EXCEPTION", "removeManpower", payload.toString(), ex.getMessage(), phoneNumber, -1);
            logService.log(logEntry);
            return StringConstants.SERVER_UNDER_MAINTENANCE;
        }
    }

    @RequestMapping(value = "vehicle/{phoneNumber}", method = RequestMethod.GET)
    public String getUserVehicles(@PathVariable("phoneNumber") String phoneNumber){

        LogEntry logEntry = new LogEntry(CLASS_NAME, Level.INFO.toString(), "ENTRY", "getUserVehicles", phoneNumber, "TODO", phoneNumber, -1);
        logService.log(logEntry);

        try{
            if(phoneNumber == null) {
                logEntry = new LogEntry(CLASS_NAME, Level.INFO.toString(), "INVALID_INPUT", "getUserVehicles", phoneNumber, StringConstants.REQUIRED_INPUTS_ARE_NOT_PROVIDED, phoneNumber, -1);
                logService.log(logEntry);
                return StringConstants.REQUIRED_INPUTS_ARE_NOT_PROVIDED;
            }

            if(!userService.isUserRegistered(phoneNumber)){
                logEntry = new LogEntry(CLASS_NAME, Level.INFO.toString(), "INVALID_INPUT", "getUserVehicles", phoneNumber, StringConstants.USER_NOT_REGISTERED, phoneNumber, -1);
                logService.log(logEntry);
                return StringConstants.USER_NOT_REGISTERED;
            }

            int userId = userService.getUserId(phoneNumber);
            int companyId = userService.getUserCompanyId(phoneNumber);
            String response = vehicleService.getUserVehicles(userId,companyId);
            logEntry = new LogEntry(CLASS_NAME, Level.INFO.toString(), "EXIT", "getUserVehicles", phoneNumber, response, phoneNumber, response.length());
            logService.log(logEntry);
            return response;

        }catch(Exception ex){
            logEntry = new LogEntry(CLASS_NAME, Level.INFO.toString(), "EXCEPTION", "removeManpower", phoneNumber, ex.getMessage(), phoneNumber, -1);
            logService.log(logEntry);
            return StringConstants.SERVER_UNDER_MAINTENANCE;
        }
    }

    @RequestMapping(value = "login", method = RequestMethod.POST)
    public String loginUser(@RequestBody Map<String, String> payload){

        String phoneNumber = payload.get(StringConstants.phoneNumber);
        LogEntry logEntry = new LogEntry(CLASS_NAME, Level.INFO.toString(), "ENTRY", "isValidUserPassword", payload.toString(), "TODO", phoneNumber, -1);
        logService.log(logEntry);

        try{
            String password = payload.get(StringConstants.password);

            if(null == phoneNumber || null == password || ("").equals(phoneNumber.trim()) || ("").equals(password.trim())) {
                logEntry = new LogEntry(CLASS_NAME, Level.INFO.toString(), "INVALID_INPUT", "isValidUserPassword", payload.toString(), StringConstants.REQUIRED_INPUTS_ARE_NOT_PROVIDED, phoneNumber, -1);
                logService.log(logEntry);
                return StringConstants.REQUIRED_INPUTS_ARE_NOT_PROVIDED;
            }

            if(!userService.isUserRegistered(phoneNumber)){
                logEntry = new LogEntry(CLASS_NAME, Level.INFO.toString(), "INVALID_INPUT", "isValidUserPassword", payload.toString(), StringConstants.USER_NOT_REGISTERED, phoneNumber, -1);
                logService.log(logEntry);
                return StringConstants.USER_NOT_REGISTERED;
            }

            password = StringMagician.getInstance().getEncryptedString(password);
            User user = new User(phoneNumber, "", password);
            String result = userService.isValidUserPassword(user);
            if(result != StringConstants.SUCCESS) {
                logEntry = new LogEntry(CLASS_NAME, Level.INFO.toString(), "INVALID_INPUT", "isValidUserPassword", payload.toString(), result, phoneNumber, -1);
                logService.log(logEntry);
                return result;
            }

            int userId = userService.getUserId(phoneNumber);
            String response = userService.getUserDetails(userId);
            logEntry = new LogEntry(CLASS_NAME, Level.INFO.toString(), "EXIT", "isValidUserPassword", payload.toString(), response, phoneNumber, response.length());
            logService.log(logEntry);
            return response;

        }catch(Exception ex){
            logEntry = new LogEntry(CLASS_NAME, Level.INFO.toString(), "EXCEPTION", "isValidUserPassword", payload.toString(), ex.getMessage(), phoneNumber, -1);
            logService.log(logEntry);
            return StringConstants.SERVER_UNDER_MAINTENANCE;
        }
    }

    @RequestMapping(value = "change_password", method = RequestMethod.POST)
    public String changePassword(@RequestBody Map<String, String> payload){

        String phoneNumber = payload.get(StringConstants.phoneNumber);
        LogEntry logEntry = new LogEntry(CLASS_NAME, Level.INFO.toString(), "ENTRY", "changePassword", payload.toString(), "TODO", phoneNumber, -1);
        logService.log(logEntry);

        try{
            String password = payload.get(StringConstants.password);
            String oldPassword = payload.get(StringConstants.oldPassword);

            if(null == phoneNumber || null == oldPassword || null == password
                    || ("").equals(phoneNumber.trim()) || ("").equals(oldPassword.trim()) || ("").equals(password.trim())) {
                logEntry = new LogEntry(CLASS_NAME, Level.INFO.toString(), "INVALID_INPUT", "changePassword", payload.toString(), StringConstants.REQUIRED_INPUTS_ARE_NOT_PROVIDED, phoneNumber, -1);
                logService.log(logEntry);
                return StringConstants.REQUIRED_INPUTS_ARE_NOT_PROVIDED;
            }


            if(!userService.isUserRegistered(phoneNumber)){
                logEntry = new LogEntry(CLASS_NAME, Level.INFO.toString(), "INVALID_INPUT", "changePassword", payload.toString(), StringConstants.USER_NOT_REGISTERED, phoneNumber, -1);
                logService.log(logEntry);
                return StringConstants.USER_NOT_REGISTERED;
            }

            oldPassword = StringMagician.getInstance().getEncryptedString(oldPassword);
            User user = new User(phoneNumber, "", oldPassword);
            String result = userService.isValidUserPassword(user);
            if(result != StringConstants.SUCCESS) {
                logEntry = new LogEntry(CLASS_NAME, Level.INFO.toString(), "INVALID_INPUT", "changePassword", payload.toString(), result, phoneNumber, -1);
                logService.log(logEntry);
                return result;
            }

            int userId = userService.getUserId(phoneNumber);
            password = StringMagician.getInstance().getEncryptedString(password);
            String response = userService.changePassword(userId, password);
            logEntry = new LogEntry(CLASS_NAME, Level.INFO.toString(), "EXIT", "changePassword", payload.toString(), response, phoneNumber, response.length());
            logService.log(logEntry);
            return response;

        }catch(Exception ex){
            logEntry = new LogEntry(CLASS_NAME, Level.INFO.toString(), "EXCEPTION", "changePassword", payload.toString(), ex.getMessage(), phoneNumber, -1);
            logService.log(logEntry);
            return StringConstants.SERVER_UNDER_MAINTENANCE;
        }
    }

    @RequestMapping(value = "tn_points/{phoneNumber}", method = RequestMethod.GET)
    public String getUserTnPoints(@PathVariable("phoneNumber") String phoneNumber){

        LogEntry logEntry = new LogEntry(CLASS_NAME, Level.INFO.toString(), "ENTRY", "getUserTnPoints", phoneNumber, "TODO", phoneNumber, -1);
        logService.log(logEntry);

        try{
            if(phoneNumber == null) {
                logEntry = new LogEntry(CLASS_NAME, Level.INFO.toString(), "INVALID_INPUT", "getUserTnPoints", phoneNumber, StringConstants.REQUIRED_INPUTS_ARE_NOT_PROVIDED, phoneNumber, -1);
                logService.log(logEntry);
                return StringConstants.REQUIRED_INPUTS_ARE_NOT_PROVIDED;
            }

            if(!userService.isUserRegistered(phoneNumber)){
                logEntry = new LogEntry(CLASS_NAME, Level.INFO.toString(), "INVALID_INPUT", "getUserTnPoints", phoneNumber, StringConstants.USER_NOT_REGISTERED, phoneNumber, -1);
                logService.log(logEntry);
                return StringConstants.USER_NOT_REGISTERED;
            }

            Integer tnPoints = userService.getUserTnPoints(phoneNumber);
            logEntry = new LogEntry(CLASS_NAME, Level.INFO.toString(), "EXIT", "getUserTnPoints", phoneNumber, tnPoints.toString(), phoneNumber, tnPoints);
            logService.log(logEntry);
            return tnPoints.toString();

        }catch(Exception ex){
            logEntry = new LogEntry(CLASS_NAME, Level.INFO.toString(), "EXCEPTION", "getUserTnPoints", phoneNumber, ex.getMessage(), phoneNumber, -1);
            logService.log(logEntry);
            return StringConstants.SERVER_UNDER_MAINTENANCE;
        }
    }

    @RequestMapping(value = "user_contacted", method = RequestMethod.POST)
    public void userContacted(@RequestBody Map<String, String> payload){

        String phoneNumber = payload.get(StringConstants.phoneNumber);
        LogEntry logEntry = new LogEntry(CLASS_NAME, Level.INFO.toString(), "ENTRY", "userContacted", payload.toString(), "TODO", phoneNumber, -1);
        logService.log(logEntry);

        try{
            String referencePhoneNumber = payload.get(StringConstants.referencePhoneNumber);
            if(referencePhoneNumber == null || referencePhoneNumber.length() == 0 || referencePhoneNumber == phoneNumber ){
                logEntry = new LogEntry(CLASS_NAME, Level.INFO.toString(), "INVALID_INPUT", "userContacted", payload.toString(), StringConstants.REQUIRED_INPUTS_ARE_NOT_PROVIDED, phoneNumber, -1);
                logService.log(logEntry);
                return;
            }

            User user = new User(phoneNumber);
            user.setReferencePhoneNumber(referencePhoneNumber);
            userService.updateUserContactCount(user);
            logEntry = new LogEntry(CLASS_NAME, Level.INFO.toString(), "EXIT", "userContacted", payload.toString(), "0", phoneNumber, 0);
            logService.log(logEntry);
        }catch(Exception ex){
            logEntry = new LogEntry(CLASS_NAME, Level.INFO.toString(), "EXCEPTION", "userContacted", payload.toString(), ex.getMessage(), phoneNumber, -1);
            logService.log(logEntry);
        }
    }
}
