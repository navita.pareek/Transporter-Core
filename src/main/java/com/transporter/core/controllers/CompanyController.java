package com.transporter.core.controllers;

import com.transporter.core.constants.StringConstants;
import com.transporter.core.models.Company;
import com.transporter.core.models.LogEntry;
import com.transporter.core.models.User;
import com.transporter.core.services.CompanyService;
import com.transporter.core.services.LogService;
import com.transporter.core.services.UserService;
import com.transporter.core.services.VehicleService;
import com.transporter.core.tools.ListToStringConverter;
import com.transporter.core.tools.StringToJsonSerializedString;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;
import java.util.logging.Level;

@RestController
@RequestMapping("/company")
public class CompanyController {

    @Autowired
    private UserService userService;

    @Autowired
    private CompanyService companyService;

    @Autowired
    private VehicleService vehicleService;

    @Autowired
    private LogService logService;

    private  final String CLASS_NAME = "com.transporter.core.controllers.CompanyController";

    @RequestMapping(value = "include_vehicles", method = RequestMethod.POST)
    public String includeUserVehiclesInCompany(@RequestBody Map<String, String> payload){

        String phoneNumber = payload.get(StringConstants.phoneNumber);
        LogEntry logEntry = new LogEntry(CLASS_NAME, Level.INFO.toString(), "ENTRY", "includeVehiclesInCompany", payload.toString(), "TODO", phoneNumber, -1);
        logService.log(logEntry);

        try{
            if(!userService.isUserRegistered(phoneNumber)){
                logEntry = new LogEntry(CLASS_NAME, Level.INFO.toString(), "INVALID_INPUT", "includeVehiclesInCompany", payload.toString(), StringConstants.USER_NOT_REGISTERED, phoneNumber, -1);
                logService.log(logEntry);
                return StringConstants.USER_NOT_REGISTERED;
            }

            int userId = userService.getUserId(phoneNumber);
            int companyId = userService.getUserCompanyId(phoneNumber);

            if(companyId < 1) {
                logEntry = new LogEntry(CLASS_NAME, Level.INFO.toString(), "INVALID_INPUT", "includeVehiclesInCompany", payload.toString(), StringConstants.REQUIRED_INPUTS_ARE_NOT_PROVIDED, phoneNumber, -1);
                logService.log(logEntry);
                return StringConstants.COMPANY_NOT_REGISTERED;
            }

            String response = companyService.includeUserVehiclesInCompany(userId, companyId);
            logEntry = new LogEntry(CLASS_NAME, Level.INFO.toString(), "EXIT", "includeVehiclesInCompany", payload.toString(), response, phoneNumber, response.length());
            logService.log(logEntry);
            return response;

        }catch(Exception ex){
            logEntry = new LogEntry(CLASS_NAME, Level.INFO.toString(), "EXCEPTION", "includeVehiclesInCompany", payload.toString(), ex.getMessage(), phoneNumber, -1);
            logService.log(logEntry);
            return StringConstants.SERVER_UNDER_MAINTENANCE;
        }
    }

    @RequestMapping(value = "add", method = RequestMethod.POST)
    public String addCompany(@RequestBody Map<String, String> payload){

        String phoneNumber = payload.get(StringConstants.phoneNumber);
        LogEntry logEntry = new LogEntry(CLASS_NAME, Level.INFO.toString(), "ENTRY", "addCompany", payload.toString(), "TODO", phoneNumber, -1);
        logService.log(logEntry);

        try{
            if(!userService.isUserRegistered(phoneNumber)){
                logEntry = new LogEntry(CLASS_NAME, Level.INFO.toString(), "INVALID_INPUT", "addCompany", payload.toString(), StringConstants.USER_NOT_REGISTERED, phoneNumber, -1);
                logService.log(logEntry);
                return StringConstants.USER_NOT_REGISTERED;
            }

            String companyName = payload.get(StringConstants.companyName);
            if(null == companyName) {
                logEntry = new LogEntry(CLASS_NAME, Level.INFO.toString(), "INVALID_INPUT", "addCompany", payload.toString(), StringConstants.REQUIRED_INPUTS_ARE_NOT_PROVIDED, phoneNumber, -1);
                logService.log(logEntry);
                return StringConstants.REQUIRED_INPUTS_ARE_NOT_PROVIDED;
            }

            if(companyService.isCompanyRegistered(companyName)){
                String response = handleAlreadyAddedCompany(companyName, phoneNumber);
                logEntry = new LogEntry(CLASS_NAME, Level.INFO.toString(), "EXIT", "addCompany", payload.toString(), response, phoneNumber, response.length());
                logService.log(logEntry);
                return response;
            }

            Company company = new Company(companyName);
            String response = handleNewCompany(company, phoneNumber);
            logEntry = new LogEntry(CLASS_NAME, Level.INFO.toString(), "EXIT", "addCompany", payload.toString(), response, phoneNumber, response.length());
            logService.log(logEntry);
            return response;

        }catch(Exception ex){
            logEntry = new LogEntry(CLASS_NAME, Level.INFO.toString(), "EXCEPTION", "addCompany", payload.toString(), ex.getMessage(), phoneNumber, -1);
            logService.log(logEntry);
            return StringConstants.SERVER_UNDER_MAINTENANCE;
        }
    }

    @RequestMapping(value = "update", method = RequestMethod.POST)
    public String updateCompany(@RequestBody Map<String, String> payload){

        String phoneNumber = payload.get(StringConstants.phoneNumber);
        LogEntry logEntry = new LogEntry(CLASS_NAME, Level.INFO.toString(), "ENTRY", "updateCompany", payload.toString(), "TODO", phoneNumber, -1);
        logService.log(logEntry);

        try{
            if(!userService.isUserRegistered(phoneNumber)){
                logEntry = new LogEntry(CLASS_NAME, Level.INFO.toString(), "INVALID_INPUT", "updateCompany", payload.toString(), StringConstants.USER_NOT_REGISTERED, phoneNumber, -1);
                logService.log(logEntry);
                return StringConstants.USER_NOT_REGISTERED;
            }

            String newCompanyName = payload.get(StringConstants.companyName);
            if(null == newCompanyName || newCompanyName.trim().length() == 0) {
                logEntry = new LogEntry(CLASS_NAME, Level.INFO.toString(), "INVALID_INPUT", "updateCompany", payload.toString(), StringConstants.REQUIRED_INPUTS_ARE_NOT_PROVIDED, phoneNumber, -1);
                logService.log(logEntry);
                return StringConstants.REQUIRED_INPUTS_ARE_NOT_PROVIDED;
            }

            if(companyService.isCompanyRegistered(newCompanyName) && !isRequesterCompany(phoneNumber, newCompanyName)){
                    logEntry = new LogEntry(CLASS_NAME, Level.INFO.toString(), "EXIT", "updateCompany", payload.toString(), StringConstants.COMPANY_REGISTERED, phoneNumber, -1);
                    logService.log(logEntry);
                    return StringConstants.COMPANY_REGISTERED;
            }

            int companyId = userService.getUserCompanyId(phoneNumber);
            companyService.updateCompany(companyId, newCompanyName);
            logEntry = new LogEntry(CLASS_NAME, Level.INFO.toString(), "EXIT", "updateCompany", payload.toString(), StringConstants.SUCCESS, phoneNumber, StringConstants.SUCCESS.length());
            logService.log(logEntry);
            return StringConstants.SUCCESS;

        }catch(Exception ex){
            logEntry = new LogEntry(CLASS_NAME, Level.INFO.toString(), "EXCEPTION", "updateCompany", payload.toString(), ex.getMessage(), phoneNumber, -1);
            logService.log(logEntry);
            return StringConstants.SERVER_UNDER_MAINTENANCE;
        }
    }

    private boolean isRequesterCompany(String phoneNumber, String newCompanyName) {
        int companyId = userService.getUserCompanyId(phoneNumber);
        String userOldCompanyName = companyService.getCompanyNameFromId(companyId);
        return newCompanyName.replaceAll(" ", "").toUpperCase().equals(userOldCompanyName.replaceAll(" ", "").toUpperCase());
    }

    @RequestMapping(value = "remove", method = RequestMethod.POST)
    public String removeCompany(@RequestBody Map<String, String> payload){

        String phoneNumber = payload.get(StringConstants.phoneNumber);
        LogEntry logEntry = new LogEntry(CLASS_NAME, Level.INFO.toString(), "ENTRY", "removeCompany", payload.toString(), "TODO", phoneNumber, -1);
        logService.log(logEntry);

        try{
            if(!userService.isUserRegistered(phoneNumber)){
                logEntry = new LogEntry(CLASS_NAME, Level.INFO.toString(), "INVALID_INPUT", "removeCompany", payload.toString(), StringConstants.USER_NOT_REGISTERED, phoneNumber, -1);
                logService.log(logEntry);
                return StringConstants.USER_NOT_REGISTERED;
            }

            int companyId = userService.getUserCompanyId(phoneNumber);
            int empoweredUserCountWorkingInCompany = userService.getUsersCountWorkingForCompany(companyId);
            String response = userService.removeUserCompany(phoneNumber);

            if(empoweredUserCountWorkingInCompany == 1){
                userService.removeCompanyUsers(companyId);
                vehicleService.removeCompanyVehicles(companyId);
                companyService.removeCompany(companyId);
            }

            logEntry = new LogEntry(CLASS_NAME, Level.INFO.toString(), "EXIT", "removeCompany", payload.toString(), response, phoneNumber, response.length());
            logService.log(logEntry);
            return response;

        }catch(Exception ex){
            logEntry = new LogEntry(CLASS_NAME, Level.INFO.toString(), "EXCEPTION", "removeCompany", payload.toString(), ex.getMessage(), phoneNumber, -1);
            logService.log(logEntry);
            return StringConstants.SERVER_UNDER_MAINTENANCE;
        }
    }

    @RequestMapping(value = "{phone_number}", method = RequestMethod.GET)
    public String getCompany(@PathVariable("phone_number") String phoneNumber){

        LogEntry logEntry = new LogEntry(CLASS_NAME, Level.INFO.toString(), "ENTRY", "getCompany", phoneNumber, "TODO", phoneNumber, -1);
        logService.log(logEntry);

        try{
            if(!userService.isUserRegistered(phoneNumber)){
                logEntry = new LogEntry(CLASS_NAME, Level.INFO.toString(), "INVALID_INPUT", "getCompany", phoneNumber, StringConstants.USER_NOT_REGISTERED, phoneNumber, -1);
                logService.log(logEntry);
                return StringConstants.USER_NOT_REGISTERED;
            }

            int companyId = userService.getUserCompanyId(phoneNumber);
            if(companyId > 0)
                return companyService.getCompanyNameFromId(companyId);
            return "";

        }catch(Exception ex){
            logEntry = new LogEntry(CLASS_NAME, Level.INFO.toString(), "EXCEPTION", "getCompany", phoneNumber, ex.getMessage(), phoneNumber, -1);
            logService.log(logEntry);
            return StringConstants.SERVER_UNDER_MAINTENANCE;
        }
    }

    @RequestMapping(value = "isEmpowered/{phone_number}", method = RequestMethod.GET)
    public String isEmpoweredToAddMembers(@PathVariable("phone_number") String phoneNumber){

        LogEntry logEntry = new LogEntry(CLASS_NAME, Level.INFO.toString(), "ENTRY", "isEmpoweredToAddMembers", phoneNumber, "TODO", phoneNumber, -1);
        logService.log(logEntry);

        try{
            if(!userService.isUserRegistered(phoneNumber)){
                logEntry = new LogEntry(CLASS_NAME, Level.INFO.toString(), "INVALID_INPUT", "isEmpoweredToAddMembers", phoneNumber, StringConstants.USER_NOT_REGISTERED, phoneNumber, -1);
                logService.log(logEntry);
                return StringConstants.USER_NOT_REGISTERED;
            }

            int companyId = userService.getUserCompanyId(phoneNumber);
            if(companyId > 0)
                return userService.getIsEmpoweredToAddMembers(phoneNumber);
            return "false";

        }catch(Exception ex){
            logEntry = new LogEntry(CLASS_NAME, Level.INFO.toString(), "EXCEPTION", "isEmpoweredToAddMembers", phoneNumber, ex.getMessage(), phoneNumber, -1);
            logService.log(logEntry);
            return StringConstants.SERVER_UNDER_MAINTENANCE;
        }
    }

    @RequestMapping(value = "members/{phone_number}", method = RequestMethod.GET)
    public String getCompanyMembers(@PathVariable("phone_number") String phoneNumber){

        LogEntry logEntry = new LogEntry(CLASS_NAME, Level.INFO.toString(), "ENTRY", "getCompanyMembers", phoneNumber, "TODO", phoneNumber, -1);
        logService.log(logEntry);

        try{
            if(!userService.isUserRegistered(phoneNumber)){
                logEntry = new LogEntry(CLASS_NAME, Level.INFO.toString(), "INVALID_INPUT", "getCompanyMembers", phoneNumber, StringConstants.USER_NOT_REGISTERED, phoneNumber, -1);
                logService.log(logEntry);
                return StringConstants.USER_NOT_REGISTERED;
            }

            int companyId = userService.getUserCompanyId(phoneNumber);
            if(companyId > 0) {
                List<User> users = userService.getCompanyMembers(companyId);
                String result = ListToStringConverter.getInstance().convert(users);
                result = StringToJsonSerializedString.ConvertToJsonSerializedString(result, StringConstants.users);
                return result;
            }
            return "";

        }catch(Exception ex){
            logEntry = new LogEntry(CLASS_NAME, Level.INFO.toString(), "EXCEPTION", "getCompanyMembers", phoneNumber, ex.getMessage(), phoneNumber, -1);
            logService.log(logEntry);
            return StringConstants.SERVER_UNDER_MAINTENANCE;
        }
    }

    private String handleNewCompany(Company company, String phoneNumber) {
        companyService.addCompany(company);
        return userService.addCompany(company, phoneNumber);
    }

    private String handleAlreadyAddedCompany(String companyName, String phoneNumber) {

        int companyId = companyService.getCompanyIdFromName(companyName);
        if(companyId < 1)
            return StringConstants.COMPANY_NOT_REGISTERED;

        String contactNumber = userService.getContactNumberToAddManpower(companyId);
        if(contactNumber.contains(phoneNumber))
            return StringConstants.DUPLICATE_REQUEST;

        if(contactNumber == null || contactNumber.length() < 1)
            return StringConstants.COMPANY_REGISTERED;

        String result = StringConstants.COMPANY_REGISTERED;
        return result;
    }
}
