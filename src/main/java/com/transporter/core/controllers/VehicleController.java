package com.transporter.core.controllers;

import com.transporter.core.constants.StringConstants;
import com.transporter.core.models.LogEntry;
import com.transporter.core.models.Vehicle;
import com.transporter.core.services.LogService;
import com.transporter.core.services.UserService;
import com.transporter.core.services.VehicleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;
import java.util.logging.Level;

@RestController
@RequestMapping("/vehicle")
public class VehicleController {

    @Autowired
    private LogService logService;

    @Autowired
    private VehicleService vehicleService;

    @Autowired
    private UserService userService;

    private  final String CLASS_NAME = "com.transporter.core.controllers.VehicleController";

    @RequestMapping(value = "add", method = RequestMethod.POST)
    public String addVehicle(@RequestBody Map<String, String> payload){

        String phoneNumber = payload.get(StringConstants.phoneNumber);
        LogEntry logEntry = new LogEntry(CLASS_NAME, Level.INFO.toString(), "ENTRY", "addVehicle", payload.toString(), "TODO", phoneNumber, -1);
        logService.log(logEntry);

        try{
            String vehicleNumber = payload.get(StringConstants.vehicleNumber);
            String capacity = payload.get(StringConstants.capacity);

            if(null == phoneNumber || null == vehicleNumber || vehicleNumber.trim().length() == 0
                    || null == capacity) {
                logEntry = new LogEntry(CLASS_NAME, Level.INFO.toString(), "INVALID_INPUT", "addVehicle", payload.toString(), StringConstants.REQUIRED_INPUTS_ARE_NOT_PROVIDED, phoneNumber, -1);
                logService.log(logEntry);
                return StringConstants.REQUIRED_INPUTS_ARE_NOT_PROVIDED;
            }

            if(!userService.isUserRegistered(phoneNumber)){
                logEntry = new LogEntry(CLASS_NAME, Level.INFO.toString(), "INVALID_INPUT", "addVehicle", payload.toString(), StringConstants.USER_NOT_REGISTERED, phoneNumber, -1);
                logService.log(logEntry);
                return StringConstants.USER_NOT_REGISTERED;
            }

            if(vehicleService.isVehicleAdded(vehicleNumber.replace(" ", ""))){
                logEntry = new LogEntry(CLASS_NAME, Level.INFO.toString(), "INVALID_INPUT", "addVehicle", payload.toString(), StringConstants.VEHICLE_ALREADY_REGISTERED, phoneNumber, -1);
                logService.log(logEntry);
                return StringConstants.VEHICLE_ALREADY_REGISTERED;
            }

            String startStateIndex = payload.get(StringConstants.startState);
            String endStateIndex = payload.get(StringConstants.endState);
            String currentStatus = payload.get(StringConstants.status);
            String isLinkedToCompany = (payload.get(StringConstants.isLinkedToCompany) != null) ? payload.get(StringConstants.isLinkedToCompany) : "false";

            String startCityIndex = payload.get(StringConstants.startCity);
            String endCityIndex = payload.get(StringConstants.endCity);
            String startLocation = startStateIndex + ":" + startCityIndex;
            String endLocation = endStateIndex + ":" + endCityIndex;
            Vehicle vehicle = new Vehicle(vehicleNumber.replace(" ", ""), getPrecisedFloat(capacity));
            vehicle.setIsLinkedToCompany(isLinkedToCompany);
            vehicle.setStartLocation(startLocation);
            vehicle.setEndLocation(endLocation);

            if(null != currentStatus) {
                if(currentStatus.equals("0"))
                    currentStatus = "1";
                vehicle.setStatus(currentStatus);
            }

            String response = vehicleService.addVehicle(vehicle, phoneNumber);
            logEntry = new LogEntry(CLASS_NAME, Level.INFO.toString(), "EXIT", "addVehicle", payload.toString(), response, phoneNumber, response.length());
            logService.log(logEntry);

            userService.addToTNPoints(phoneNumber, StringConstants.vehicleAddPoints);
            return response;

        }catch(Exception ex){
            logEntry = new LogEntry(CLASS_NAME, Level.INFO.toString(), "EXCEPTION", "addVehicle", payload.toString(), ex.getMessage(), phoneNumber, -1);
            logService.log(logEntry);
            return StringConstants.SERVER_UNDER_MAINTENANCE;
        }
    }

    @RequestMapping(value = "update", method = RequestMethod.POST)
    public String updateVehicle(@RequestBody Map<String, String> payload){

        String phoneNumber = payload.get(StringConstants.phoneNumber);
        LogEntry logEntry = new LogEntry(CLASS_NAME, Level.INFO.toString(), "ENTRY", "updateVehicle", payload.toString(), "TODO", phoneNumber, -1);
        logService.log(logEntry);

        try{
            String vehicleNumber = payload.get(StringConstants.vehicleNumber);
            if(!vehicleService.isVehicleAdded(vehicleNumber)){
                logEntry = new LogEntry(CLASS_NAME, Level.INFO.toString(), "INVALID_INPUT", "updateVehicle", payload.toString(), StringConstants.VEHICLE_NOT_REGISTERED, phoneNumber, -1);
                logService.log(logEntry);
                return StringConstants.VEHICLE_NOT_REGISTERED;
            }

            String currentStateIndex = payload.get(StringConstants.currentState);
            String currentCityIndex = payload.get(StringConstants.currentCity);

            String currentLocation = currentStateIndex + ":" + currentCityIndex;

            String endStateIndex = payload.get(StringConstants.endState);
            String endCityIndex = payload.get(StringConstants.endCity);
            String endLocation = endStateIndex + ":" + endCityIndex;

            String currentStatusIndex = payload.get(StringConstants.status);
            if(currentStatusIndex.equals("0"))
                currentStatusIndex = "1";

            String vehicleCapacity = payload.get(StringConstants.capacity);

            Vehicle vehicle = new Vehicle(vehicleNumber, getPrecisedFloat(vehicleCapacity));

            vehicle.setStartLocation(currentLocation);
            vehicle.setEndLocation(endLocation);
            vehicle.setStatus(currentStatusIndex);

            String response = vehicleService.updateVehicle(vehicle);
            logEntry = new LogEntry(CLASS_NAME, Level.INFO.toString(), "EXIT", "updateVehicle", payload.toString(), response, phoneNumber, response.length());
            logService.log(logEntry);
            return response;

        }catch(Exception ex){
            logEntry = new LogEntry(CLASS_NAME, Level.INFO.toString(), "EXCEPTION", "updateVehicle", payload.toString(), ex.getMessage(), phoneNumber, -1);
            logService.log(logEntry);
            return StringConstants.SERVER_UNDER_MAINTENANCE;
        }
    }

    @RequestMapping(value = "remove", method = RequestMethod.POST)
    public String removeVehicle(@RequestBody Map<String, String> payload){

        String phoneNumber = payload.get(StringConstants.phoneNumber);
        LogEntry logEntry = new LogEntry(CLASS_NAME, Level.INFO.toString(), "ENTRY", "removeVehicle", payload.toString(), "TODO", phoneNumber, -1);
        logService.log(logEntry);

        try{
            String vehicleNumber = payload.get(StringConstants.vehicleNumber);
            if(!vehicleService.isVehicleAdded(vehicleNumber)){
                logEntry = new LogEntry(CLASS_NAME, Level.INFO.toString(), "INVALID_INPUT", "removeVehicle", payload.toString(), StringConstants.VEHICLE_NOT_REGISTERED, phoneNumber, -1);
                logService.log(logEntry);
                return StringConstants.VEHICLE_NOT_REGISTERED;
            }

            if(!userService.isUserRegistered(phoneNumber)){
                logEntry = new LogEntry(CLASS_NAME, Level.INFO.toString(), "INVALID_INPUT", "removeVehicle", payload.toString(), StringConstants.USER_NOT_REGISTERED, phoneNumber, -1);
                logService.log(logEntry);
                return StringConstants.USER_NOT_REGISTERED;
            }

            int userId = userService.getUserId(phoneNumber);
            if(!vehicleService.isVehicleAdded(vehicleNumber, userId)) {
                logEntry = new LogEntry(CLASS_NAME, Level.INFO.toString(), "INVALID_INPUT", "removeVehicle", payload.toString(), StringConstants.VEHICLE_NOT_REGISTERED, phoneNumber, -1);
                logService.log(logEntry);
                return StringConstants.VEHICLE_NOT_REGISTERED;
            }

            String response = vehicleService.removeVehicle(vehicleNumber, userId);
            logEntry = new LogEntry(CLASS_NAME, Level.INFO.toString(), "EXIT", "removeVehicle", payload.toString(), response, phoneNumber, response.length());
            logService.log(logEntry);
            return response;

        }catch(Exception ex){
            logEntry = new LogEntry(CLASS_NAME, Level.INFO.toString(), "EXCEPTION", "removeVehicle", payload.toString(), ex.getMessage(), phoneNumber, -1);
            logService.log(logEntry);
            return StringConstants.SERVER_UNDER_MAINTENANCE;
        }
    }

    private float getPrecisedFloat(String inputString) {
        Float stringToFloat = Float.parseFloat(inputString);
        String precisedString = String.format(StringConstants.FLOAT_PRECISION,stringToFloat);
        return Float.parseFloat(precisedString);
    }
}
