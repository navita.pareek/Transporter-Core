package com.transporter.core.controllers;

import com.transporter.core.constants.StringConstants;
import com.transporter.core.models.LogEntry;
import com.transporter.core.models.Trip;
import com.transporter.core.services.LogService;
import com.transporter.core.services.TripService;
import com.transporter.core.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;
import java.util.logging.Level;

@RestController
@RequestMapping("/trip")
public class TripController {

    @Autowired
    private UserService userService;

    @Autowired
    private TripService tripService;

    @Autowired
    private LogService logService;

    private  final String CLASS_NAME = "com.transporter.core.controllers.TripController";

    @RequestMapping(value = "add", method = RequestMethod.POST)
    public String addTrip(@RequestBody Map<String, String> payload){

        String phoneNumber = payload.get(StringConstants.phoneNumber);
        LogEntry logEntry = new LogEntry(CLASS_NAME, Level.INFO.toString(), "ENTRY", "addTrip", payload.toString(), "TODO", phoneNumber, -1);
        logService.log(logEntry);

        try{
            String startDate = payload.get(StringConstants.startDate);
            String startStateIndex = payload.get(StringConstants.startState);
            String endStateIndex = payload.get(StringConstants.endState);
            String vehicleCapacityRequired = payload.get(StringConstants.vehicleCapacityRequired);
            String startCityIndex = payload.get(StringConstants.startCity);
            String endCityIndex = payload.get(StringConstants.endCity);

            if(null == phoneNumber || null == startDate ||
                    "0" == startStateIndex || "0" == endStateIndex ||
                    "0" == startCityIndex || "0" == endCityIndex ||
                    null == vehicleCapacityRequired) {
                logEntry = new LogEntry(CLASS_NAME, Level.INFO.toString(), "INVALID_INPUT", "addTrip", payload.toString(), StringConstants.REQUIRED_INPUTS_ARE_NOT_PROVIDED, phoneNumber, -1);
                logService.log(logEntry);
                return StringConstants.REQUIRED_INPUTS_ARE_NOT_PROVIDED;
            }

            if(!userService.isUserRegistered(phoneNumber)){
                logEntry = new LogEntry(CLASS_NAME, Level.INFO.toString(), "INVALID_INPUT", "addTrip", payload.toString(), StringConstants.USER_NOT_REGISTERED, phoneNumber, -1);
                logService.log(logEntry);
                return StringConstants.USER_NOT_REGISTERED;
            }

            String tripStatus = StringConstants.trip_searching_for_vehicle;
            String tripDescription = payload.get(StringConstants.tripDescription);

            Trip trip = new Trip();
            trip.setStartDate(startDate);
            trip.setStartStateIndex(startStateIndex);
            trip.setStartCityIndex(startCityIndex);
            trip.setEndStateIndex(endStateIndex);
            trip.setEndCityIndex(endCityIndex);
            trip.setVehicleCapacityRequired(getPrecisedFloat(vehicleCapacityRequired));
            trip.setTripStatus(tripStatus);
            trip.setTripDescription(tripDescription);

            int userId = userService.getUserId(phoneNumber);
            if(tripService.isTripExists(trip, userId)) {
                logEntry = new LogEntry(CLASS_NAME, Level.INFO.toString(), "EXIT", "addTrip", payload.toString(), StringConstants.TRIP_ALREADY_EXISTS, phoneNumber, -1);
                logService.log(logEntry);
                return StringConstants.TRIP_ALREADY_EXISTS;
            }

            tripService.addTrip(trip, userId);
            logEntry = new LogEntry(CLASS_NAME, Level.INFO.toString(), "EXIT", "addTrip", payload.toString(), StringConstants.SUCCESS, phoneNumber, StringConstants.SUCCESS.length());
            logService.log(logEntry);

            userService.addToTNPoints(phoneNumber, StringConstants.tripAddPoints);

            return StringConstants.SUCCESS;

        }catch(Exception ex){
            logEntry = new LogEntry(CLASS_NAME, Level.INFO.toString(), "EXCEPTION", "addTrip", payload.toString(), ex.getMessage(), phoneNumber, -1);
            logService.log(logEntry);
            return StringConstants.SERVER_UNDER_MAINTENANCE;
        }
    }

    @RequestMapping(value = "search_vehicle", method = RequestMethod.POST)
    public String searchVehicle(@RequestBody Map<String, String> payload){

        String phoneNumber = payload.get(StringConstants.phoneNumber);
        LogEntry logEntry = new LogEntry(CLASS_NAME, Level.INFO.toString(), "ENTRY", "searchVehicle", payload.toString(), "TODO", phoneNumber, -1);
        logService.log(logEntry);

        try{
            String startDate = payload.get(StringConstants.startDate);
            String startStateIndex = payload.get(StringConstants.startState);
            String endStateIndex = payload.get(StringConstants.endState);
            String vehicleCapacityRequired = payload.get(StringConstants.vehicleCapacityRequired);
            String startCityIndex = payload.get(StringConstants.startCity);
            String endCityIndex = payload.get(StringConstants.endCity);

            if(null == phoneNumber || null == startDate ||
                    "0" == startStateIndex || "0" == endStateIndex ||
                    "0" == startCityIndex || "0" == endCityIndex ||
                    null == vehicleCapacityRequired) {
                logEntry = new LogEntry(CLASS_NAME, Level.INFO.toString(), "INVALID_INPUT", "searchVehicle", payload.toString(), StringConstants.REQUIRED_INPUTS_ARE_NOT_PROVIDED, phoneNumber, -1);
                logService.log(logEntry);
                return StringConstants.REQUIRED_INPUTS_ARE_NOT_PROVIDED;
            }

            if(!userService.isUserRegistered(phoneNumber)){
                logEntry = new LogEntry(CLASS_NAME, Level.INFO.toString(), "INVALID_INPUT", "searchVehicle", payload.toString(), StringConstants.USER_NOT_REGISTERED, phoneNumber, -1);
                logService.log(logEntry);
                return StringConstants.USER_NOT_REGISTERED;
            }

            String tripStatus = StringConstants.trip_searching_for_vehicle;
            String tripDescription = payload.get(StringConstants.tripDescription);

            Trip trip = new Trip();
            trip.setStartDate(startDate);
            trip.setStartStateIndex(startStateIndex);
            trip.setStartCityIndex(startCityIndex);
            trip.setEndStateIndex(endStateIndex);
            trip.setEndCityIndex(endCityIndex);
            trip.setVehicleCapacityRequired(getPrecisedFloat(vehicleCapacityRequired));
            trip.setTripStatus(tripStatus);
            trip.setTripDescription(tripDescription);

            String response = tripService.getMatchedVehiclesContactNumber(trip);
            logEntry = new LogEntry(CLASS_NAME, Level.INFO.toString(), "EXIT", "searchVehicle", payload.toString(), response, phoneNumber, response.length());
            logService.log(logEntry);
            return response;

        }catch(Exception ex){
            logEntry = new LogEntry(CLASS_NAME, Level.INFO.toString(), "EXCEPTION", "searchVehicle", payload.toString(), ex.getMessage(), phoneNumber, -1);
            logService.log(logEntry);
            return StringConstants.SERVER_UNDER_MAINTENANCE;
        }
    }

    @RequestMapping(value = "search", method = RequestMethod.POST)
    public String searchTripForVehicle(@RequestBody Map<String, String> payload){

        String phoneNumber = payload.get(StringConstants.phoneNumber);
        LogEntry logEntry = new LogEntry(CLASS_NAME, Level.INFO.toString(), "ENTRY", "searchTripForVehicle", payload.toString(), "TODO", phoneNumber, -1);
        logService.log(logEntry);

        try{
            String startDate = payload.get(StringConstants.startDate);
            String startStateIndex = payload.get(StringConstants.startState);
            String endStateIndex = payload.get(StringConstants.endState);
            String vehicleCapacityRequired = payload.get(StringConstants.vehicleCapacityRequired);

            if(null == startDate || null == vehicleCapacityRequired) {
                logEntry = new LogEntry(CLASS_NAME, Level.INFO.toString(), "INVALID_INPUT", "searchTripForVehicle", payload.toString(), StringConstants.REQUIRED_INPUTS_ARE_NOT_PROVIDED, phoneNumber, -1);
                logService.log(logEntry);
                return StringConstants.REQUIRED_INPUTS_ARE_NOT_PROVIDED;
            }

            String startCityIndex = payload.get(StringConstants.startCity);
            String endCityIndex = payload.get(StringConstants.endCity);
            String tripStatus = StringConstants.trip_searching_for_vehicle;

            Trip trip = new Trip();
            trip.setStartDate(startDate);
            trip.setStartStateIndex(startStateIndex);
            trip.setStartCityIndex(startCityIndex);
            trip.setEndStateIndex(endStateIndex);
            trip.setEndCityIndex(endCityIndex);
            trip.setVehicleCapacityRequired(getPrecisedFloat(vehicleCapacityRequired));
            trip.setTripStatus(tripStatus);

            String response =  tripService.searchTrip(trip);
            logEntry = new LogEntry(CLASS_NAME, Level.INFO.toString(), "EXIT", "searchTripForVehicle", payload.toString(), response, phoneNumber, response.length());
            logService.log(logEntry);
            return response;

        }catch(Exception ex){
            logEntry = new LogEntry(CLASS_NAME, Level.INFO.toString(), "EXCEPTION", "addTrip", payload.toString(), ex.getMessage(), phoneNumber, -1);
            logService.log(logEntry);
            return StringConstants.SERVER_UNDER_MAINTENANCE;
        }
    }

    @RequestMapping(value = "{phoneNumber}", method = RequestMethod.GET)
    public String getUserTrips(@PathVariable("phoneNumber") String phoneNumber){

        LogEntry logEntry = new LogEntry(CLASS_NAME, Level.INFO.toString(), "ENTRY", "getUserTrips", phoneNumber, "TODO", phoneNumber, -1);
        logService.log(logEntry);

        try{
            if(phoneNumber == null) {
                logEntry = new LogEntry(CLASS_NAME, Level.INFO.toString(), "INVALID_INPUT", "getUserTrips", phoneNumber, StringConstants.REQUIRED_INPUTS_ARE_NOT_PROVIDED, phoneNumber, -1);
                logService.log(logEntry);
                return StringConstants.REQUIRED_INPUTS_ARE_NOT_PROVIDED;
            }

            if(!userService.isUserRegistered(phoneNumber)){
                logEntry = new LogEntry(CLASS_NAME, Level.INFO.toString(), "INVALID_INPUT", "getUserTrips", phoneNumber, StringConstants.USER_NOT_REGISTERED, phoneNumber, -1);
                logService.log(logEntry);
                return StringConstants.USER_NOT_REGISTERED;
            }

            int userId = userService.getUserId(phoneNumber);
            String response = tripService.getTripsByUserId(userId);
            logEntry = new LogEntry(CLASS_NAME, Level.INFO.toString(), "EXIT", "getUserTrips", phoneNumber, response, phoneNumber, response.length());
            logService.log(logEntry);
            return response;

        }catch(Exception ex){
            logEntry = new LogEntry(CLASS_NAME, Level.INFO.toString(), "EXCEPTION", "getUserTrips", phoneNumber, ex.getMessage(), phoneNumber, -1);
            logService.log(logEntry);
            return StringConstants.SERVER_UNDER_MAINTENANCE;
        }
    }

    @RequestMapping(value = "update", method = RequestMethod.POST)
    public String updateTripStatus(@RequestBody Map<String, String> payload){

        String phoneNumber = payload.get(StringConstants.phoneNumber);
        LogEntry logEntry = new LogEntry(CLASS_NAME, Level.INFO.toString(), "ENTRY", "updateTripStatus", payload.toString(), "TODO", phoneNumber, -1);
        logService.log(logEntry);

        try{
            String startDate = payload.get(StringConstants.startDate);
            String startStateIndex = payload.get(StringConstants.startState);
            String vehicleCapacityRequired = payload.get(StringConstants.vehicleCapacityRequired);
            String tripStatus = payload.get(StringConstants.status);

            String newTripStatusIndex = payload.get(StringConstants.newStatus);
            String newTripStatus = newTripStatusIndex;

            if(null == phoneNumber || null == startDate || null == startStateIndex || null == vehicleCapacityRequired || null == tripStatus || null == newTripStatus) {
                logEntry = new LogEntry(CLASS_NAME, Level.INFO.toString(), "INVALID_INPUT", "updateTripStatus", phoneNumber, StringConstants.REQUIRED_INPUTS_ARE_NOT_PROVIDED, phoneNumber, -1);
                logService.log(logEntry);
                return StringConstants.REQUIRED_INPUTS_ARE_NOT_PROVIDED;
            }

            if(!userService.isUserRegistered(phoneNumber)){
                logEntry = new LogEntry(CLASS_NAME, Level.INFO.toString(), "INVALID_INPUT", "updateTripStatus", phoneNumber, StringConstants.USER_NOT_REGISTERED, phoneNumber, -1);
                logService.log(logEntry);
                return StringConstants.USER_NOT_REGISTERED;
            }

            String startCityIndex = payload.get(StringConstants.startCity);
            String endStateIndex = payload.get(StringConstants.endState);
            String endCityIndex = payload.get(StringConstants.endCity);

            Trip trip = new Trip();
            trip.setStartDate(startDate);
            trip.setStartStateIndex(startStateIndex);
            trip.setStartCityIndex(startCityIndex);
            trip.setEndStateIndex(endStateIndex);
            trip.setEndCityIndex(endCityIndex);

            trip.setVehicleCapacityRequired(getPrecisedFloat(vehicleCapacityRequired));
            trip.setTripStatus(tripStatus);
            if(tripService.searchTrip(trip).length() > 0){
                int userId = userService.getUserId(phoneNumber);
                String response = tripService.updateTrip(trip, userId, newTripStatus);
                logEntry = new LogEntry(CLASS_NAME, Level.INFO.toString(), "EXIT", "updateTripStatus", payload.toString(), response, phoneNumber, response.length());
                logService.log(logEntry);
                return response;
            }

            String response = StringConstants.INVALID_INFORMATION_PROVIDED;
            logEntry = new LogEntry(CLASS_NAME, Level.INFO.toString(), "EXIT", "updateTripStatus", payload.toString(), response, phoneNumber, response.length());
            logService.log(logEntry);
            return response;

        }catch(Exception ex){
            logEntry = new LogEntry(CLASS_NAME, Level.INFO.toString(), "EXCEPTION", "updateTripStatus", payload.toString(), ex.getMessage(), phoneNumber, -1);
            logService.log(logEntry);
            return StringConstants.SERVER_UNDER_MAINTENANCE;
        }
    }

    private float getPrecisedFloat(String inputString) {
        Float stringToFloat = Float.parseFloat(inputString);
        String precisedString = String.format(StringConstants.FLOAT_PRECISION,stringToFloat);
        return Float.parseFloat(precisedString);
    }
}
